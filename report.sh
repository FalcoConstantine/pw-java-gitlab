#!/bin/sh

PROJECT_NAME=$1
JOB_ID=$2
#BUILD_NAME=$3
#BUILD_STATUS=$4

newUrl="https://falcoconstantine.gitlab.io/-/pw-java-gitlab/-/jobs/$JOB_ID/artifacts/allure-report/index.html"
newUrl1="https://gitlab.com/FalcoConstantine/pw-java-gitlab/-/jobs/$JOB_ID/artifacts/browse/build/"
newUrl2="https://falcoconstantine.gitlab.io/-/pw-java-gitlab/-/jobs/$JOB_ID/artifacts/build/TestExecutionReport.html"
message="*Project:* $PROJECT_NAME \n*Allure Report:* <$newUrl> \n*Report:* <$newUrl2> \n*Report:* <$newUrl2>"

#curl -H 'Content-Type: application/json' -d '{"text": "Webhook test"}' https://hooks.slack.com/services/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

#curl -H 'Content-Type: application/json' -d '{"text": "https://gitlab.com/FalcoConstantine/pw-java-gitlab/-/jobs/5768604966/artifacts/external_file/allure-report/index.html"}' https://hooks.slack.com/services/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

curl --location --request POST 'https://hooks.slack.com/services/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' \
--header 'Content-Type: application/json' \
--data-raw "{ \"text\": \"$message\", \"channel\": \"general\"}"