package com.webstore.tests.api.users;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import com.webstore.utils.Randomiser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;
import static com.webstore.pages.Constants.HttpStatus.*;
import static com.webstore.pages.Constants.Users.EMAIL;
import static com.webstore.pages.Constants.Users.NAME;

@Story("Create User with same data")
public class CreateSameUserNegativeCaseTest extends ApiBaseTest {

    Users users;
    Parser parser;

    static Random random = new Random();
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);

    public String name;
    public String email;

    private static final Logger logger = LoggerFactory.getLogger(GetUserTest.class);

    @Description("Get User details test")
    @Link("http://project.jra.lt/browse/MPM-7777")
    @Test
    public void createSameUserNegativeTest() throws IOException {

        logger.info("Create test data");
        name = randomiser.getRandomName();
        email = randomiser.getRandomEmail();

        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        users = new Users(requestContext);

        logger.info("Create new user");
        APIResponse response = users.createUser(GET_USERS, name, email);

        logger.info("Parse response");
        parser = new Parser(response.body());
        String ID = parser.responseBody().get("id").asText();

        logger.info("Verify response details");
        Assert.assertEquals(response.status(), CREATED);
        Assert.assertTrue(response.text().contains(name));
        Assert.assertTrue(response.text().contains(email));
        Assert.assertEquals(parser.responseBody().get("name").asText(), name);
        Assert.assertEquals(parser.responseBody().get("email").asText(), email);

        logger.info("Create new user with same data");
        APIResponse response1 = users.createUser(GET_USERS, name, email);

        // parse
        parser = new Parser(response1.body());

        logger.info("Verify response details");
        Assert.assertEquals(response1.status(), UNPROCESSABLE_ENTITY);
        Assert.assertTrue(response1.text().contains("field"));
        Assert.assertTrue(response1.text().contains("message"));
        Assert.assertEquals(parser.responseBody().get(0).get("message").asText(), "has already been taken");

        logger.info("Delete created User by ID");
        APIResponse deleteResponse = users.deleteUser(GET_USERS + "/" + ID);

        logger.info("Get UsersRequest list and verify if user still exist");
        APIResponse usersAfterDelete = users.getUsers(GET_USERS);
        JsonArray jsonUsersListAfterDelete = new Gson().fromJson(usersAfterDelete.text(), JsonArray.class);
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(NAME));
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(EMAIL));
    }
}