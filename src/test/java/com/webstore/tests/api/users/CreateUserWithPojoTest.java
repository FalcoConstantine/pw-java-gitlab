package com.webstore.tests.api.users;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.dao.request.UsersRequest;
import com.webstore.api.dao.response.UsersResponse;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import com.webstore.utils.Randomiser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;

@Story("Create new user and verify data")
public class CreateUserWithPojoTest extends ApiBaseTest {

    Users userService;
    Parser parser;
//    UsersResponse user;
    UsersResponse actualUser;

    UsersRequest usersRequest;

    static Random random = new Random();
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);

//    public String name;
//    public String email;
//    public String gender;
//    public String status;
//    public String userId;
    private static final Logger logger = LoggerFactory.getLogger(GetUserTest.class);

    @Description("Create new user verify if created then remove and verify if user still exist")
    @Link("http://project.jra.lt/browse/MPM-7777")
    @Test()
    public void createUserTest() throws IOException {

        logger.info("Create test data");
        // builder can be here also  instead of static test data
        usersRequest = UsersRequest.builder()
                .name(randomiser.getRandomName())
                .email(randomiser.getRandomEmail())
                .gender("male")
                .status("active")
                .build();

//        name = randomiser.getRandomName();
//        email = randomiser.getRandomEmail();
//        gender = "male";
//        status = "active";

        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        userService = new Users(requestContext);

        logger.info("Initialize new user object");
//        user = new UsersResponse(name, email, gender, status);

//        usersRequest = UsersRequest.builder()
//                .name(name)
//                .email(email)
//                .gender(gender)
//                .status(status)
//                .build();

        logger.info("Create new user");
        actualUser = userService.createUserWithPojo(GET_USERS, usersRequest);
        // provided object with params + getters/setters  and constructors
//        UsersResponse user = userService.createUserWithPojo(GET_USERS, name, email, gender, status);
        Assert.assertNotNull(actualUser.getId());
        Assert.assertEquals(actualUser.getName(), usersRequest.getName());
        Assert.assertEquals(actualUser.getEmail(), usersRequest.getEmail());
        Assert.assertEquals(actualUser.getGender(), usersRequest.getGender());
        Assert.assertEquals(actualUser.getStatus(), usersRequest.getStatus());

        logger.info("Get UsersRequest list and verify if user has been created");
        APIResponse responseBody = userService.getUsers(GET_USERS); //returns response in bytes[]
        JsonArray json = new Gson().fromJson(responseBody.text(), JsonArray.class);
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(usersRequest.getName()));
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(usersRequest.getEmail()));

        logger.info("Get ID of created user");
        parser = new Parser(responseBody.body());
        String userId = parser.responseBody().get(0).get("id").toString();

        logger.info("Update Json Object");
        //Could be like builder again
//        usersRequest = UsersRequest.builder()
//                .name(randomiser.getRandomName())
//                .email(randomiser.getRandomEmail())
//                .gender("female")
//                .status("inactive")
//                .build();

        // Like setters -- updating Object
        usersRequest.setEmail("UpdatedUser@gmail.com");
        usersRequest.setName("Updated User");
        usersRequest.setGender("female");
        usersRequest.setStatus("inactive");

        actualUser = userService.updateUserObjectWithPojo(GET_USERS, userId, usersRequest);
        Assert.assertEquals(actualUser.getName(), usersRequest.getName());
        Assert.assertEquals(actualUser.getEmail(), usersRequest.getEmail());
        Assert.assertEquals(actualUser.getGender(), usersRequest.getGender());
        Assert.assertEquals(actualUser.getStatus(), usersRequest.getStatus());

        logger.info("Get updated users list");
        APIResponse getUpdatedUsersList = userService.getUsers(GET_USERS);
        System.out.println("Updated list of users " + getUpdatedUsersList.text());

        logger.info("Delete User by ID");
        userService.deleteUserById(GET_USERS, userId);

        logger.info("Get UsersRequest list and verify if user still exist");
        APIResponse usersAfterDelete = userService.getUsers(GET_USERS);
        JsonArray jsonUsersListAfterDelete = new Gson().fromJson(usersAfterDelete.text(), JsonArray.class);
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(usersRequest.getName()));
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(usersRequest.getEmail()));
    }
}
