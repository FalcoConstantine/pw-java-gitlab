package com.webstore.tests.api.users;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import com.webstore.api.dao.response.UsersResponse;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import com.webstore.utils.Randomiser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;

@Story("Create new user and verify data")
public class CreateUserTest extends ApiBaseTest {

    Users users;
    Parser parser;

    static Random random = new Random();
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);

    public String name;
    public String email;
    public String gender;
    public String status;
    public String userId;
    private static final Logger logger = LoggerFactory.getLogger(GetUserTest.class);

    @Description("Create new user verify if created then remove and verify if user still exist")
    @Link("http://project.jra.lt/browse/MPM-7777")
    @Test()
    public void createUserTest() throws IOException {

        logger.info("Create test data");
        name = randomiser.getRandomName();
        email = randomiser.getRandomEmail();
        gender = "male";
        status = "active";

        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        users = new Users(requestContext);

        logger.info("Create new user");
        APIResponse response = users.createUser(GET_USERS, name, email, gender, status);
        parser = new Parser(response.body());
        String ID = parser.responseBody().get("id").asText();
        Assert.assertTrue(response.text().contains(name));
        Assert.assertTrue(response.text().contains(email));
        Assert.assertEquals(parser.responseBody().get("name").asText(), name);
        Assert.assertEquals(parser.responseBody().get("email").asText(), email);

        logger.info("Get UsersRequest list and verify if user has been created");
        APIResponse responseBody = users.getUsers(GET_USERS);
        JsonArray json = new Gson().fromJson(responseBody.text(), JsonArray.class);
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(name));
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(email));

        logger.info("Get ID of created user");
        parser = new Parser(responseBody.body());
        String userId = parser.responseBody().get(0).get("id").toString();

        logger.info("Delete User by ID");
        users.deleteUserById(GET_USERS, userId);

        logger.info("Get UsersRequest list and verify if user still exist");
        APIResponse usersAfterDelete = users.getUsers(GET_USERS);
        JsonArray jsonUsersListAfterDelete = new Gson().fromJson(usersAfterDelete.text(), JsonArray.class);
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(name));
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(email));
    }
}
