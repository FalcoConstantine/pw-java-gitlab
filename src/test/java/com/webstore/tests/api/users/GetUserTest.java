package com.webstore.tests.api.users;

import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;
import static com.webstore.pages.Constants.HttpStatus.OK;

@Story("Get user details by ID")
public class GetUserTest extends ApiBaseTest {
    Parser parser;
    Users users;

    private static final Logger logger = LoggerFactory.getLogger(GetUserTest.class);

    @DataProvider
    public Object[][] getUserData() {
        return new Object[][] {
                {"5806362","David Weimann"},
                {"5775128","Mrs. Chandravati Varma"},
                {"5802807", "KFA KFA"}
        };
    }

    @Description("Get User details test")
    @Link("http://project.jra.lt/browse/MPM-7777")
    @Test(dataProvider = "getUserData")
    public void getUserTest(String id, String name) throws IOException {

        logger.info("Test data");
        // from data provider

        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        users = new Users(requestContext);

        logger.info("Get user details by ID and Name");
        APIResponse response =users.getUserByIdAndName(GET_USERS, id, name);

        logger.info("Get user details by ID");
        APIResponse responseById =users.getUserById(GET_USERS, id);

        // parsing
        parser = new Parser(response.body());
        System.out.println(parser.responseBody().toPrettyString());

        logger.info("Verify the response details");
        Map<String, String> headers = response.headers();
        System.out.println(headers);
        Assert.assertEquals(headers.get("content-type"), "application/json; charset=utf-8");
        Assert.assertTrue(response.text().contains(id));
        Assert.assertTrue(response.text().contains(name));
    }
}