package com.webstore.tests.api.users;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import com.webstore.utils.Randomiser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;
import static com.webstore.pages.Constants.Jsons.USERS_JSON;
import static com.webstore.pages.Constants.Jsons.VISA;
import static com.webstore.pages.Constants.Users.EMAIL;
import static com.webstore.pages.Constants.Users.NAME;

public class TemplateTest extends ApiBaseTest {

    Users users;
    Parser parser;
    File file;

    static Random random = new Random();
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);

    public String name;
    public String email;

    public String gender;

    public String status;
    private static final Logger logger = LoggerFactory.getLogger(GetUserTest.class);

    @Test
    public void createUserTest() throws IOException {

        logger.info("Create test data");
        name = randomiser.getRandomName();
        email = randomiser.getRandomEmail();
        gender = "male";
        status = "active";
//        file = new File(VISA);
//        JsonPath.read(file, "$.name").toString();


//        Map<String, Object> data = new HashMap<>();
//        data.put("name", NAME);
//        data.put("email", EMAIL);
//        data.put("gender", "male");
//        data.put("status", "active");

//        HashMap modifyMap = new HashMap() {{
//            put("personalDetails[0].children[1].age", 9);
//            put("personalDetails[1].job", "Chiropractor");
//        }};

        // String json: (bad approach)
//        String data = "{\n" +
//                "  \"name\" : \"" + NAME + "\",\n" +
//                "  \"email\" : \"" + EMAIL +"\",\n" +
//                "  \"gender\" : \"female\",\n" +
//                "  \"status\" : \"active\"\n" +
//                "}";

//        //Upload Json file files by bites for Json payload body
//        File file = new File(USERS_JSON);
//        byte[] data = Files.readAllBytes(file.toPath());
//
//        Update and  Upload Json file as payload
//        File file = new File(USERS_JSON);
//        String jsonPayload = JsonPath.parse(file)
//                .set("$.name", NAME)
//                .set("$.email", EMAIL)
//                .jsonString();

//        JsonPath.read(file, "$.name").toString();


//        System.out.println("PAYLOAD: " + jsonPayload);


        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        users = new Users(requestContext);

        logger.info("Create new user");
        APIResponse response = users.createUser(GET_USERS, name, email, gender, status);
//        APIResponse response = users.createUser(GET_USERS, RequestOptions.create()
//                .setData(data)
//                .setTimeout(1000)
//        );
//                .text(); returns String

//        Assert.assertEquals(response.status(), CREATED);

        parser = new Parser(response.body());
        String ID = parser.responseBody().get("id").asText();
//        System.out.println("USER ID -> " + ID);
//        System.out.println("POST response: " + response.text());
//        System.out.println("POST URL: " + response.url());
//        System.out.println(parser.responseBody().toPrettyString());

        Assert.assertTrue(response.text().contains(name));
        Assert.assertTrue(response.text().contains(email));

        Assert.assertEquals(parser.responseBody().get("name").asText(), name);
        Assert.assertEquals(parser.responseBody().get("email").asText(), email);

        logger.info("Get UsersRequest list and verify if user has been created");
        APIResponse responseBody = users.getUsers(GET_USERS);
        System.out.println("GET users response: " + responseBody.text());
        System.out.println("GET URL AFTER CREATION: " + responseBody.url());
        JsonArray json = new Gson().fromJson(responseBody.text(), JsonArray.class);
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(name));
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(email));

        logger.info("Get ID of created user");
        parser = new Parser(responseBody.body());

        String userId = parser.responseBody().get(0).get("id").toString();
//        .asList().stream().findFirst().toString().trim();
        System.out.println("User ID " + userId);

        logger.info("Delete created User by ID");
//        APIResponse deleteResponse = users.deleteUser(GET_USERS + "/" + userId);
        APIResponse deleteResponse = users.deleteUserById(GET_USERS, userId);
//                .text();
//        Assert.assertEquals(deleteResponse.status(), NO_CONTENT);
//        System.out.println("Delete response body: " + deleteResponse.text());
//        System.out.println("DELETE URL: " + deleteResponse.url());

        logger.info("Get UsersRequest list and verify if user still exist");
        APIResponse usersAfterDelete = users.getUsers(GET_USERS);
        System.out.println("GET users response: " + usersAfterDelete.text());
        System.out.println("GET URL AFTER DELETE: " + usersAfterDelete.url());
        JsonArray jsonUsersListAfterDelete = new Gson().fromJson(usersAfterDelete.text(), JsonArray.class);
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(name));
        Assert.assertFalse(jsonUsersListAfterDelete.asList().stream().findFirst().toString().contains(email));
    }
}
