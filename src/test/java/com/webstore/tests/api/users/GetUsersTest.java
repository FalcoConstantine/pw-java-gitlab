package com.webstore.tests.api.users;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.services.Users;
import com.webstore.configuration.ApiFactory;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.io.IOException;
import java.util.Map;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;

@Story("Get UsersRequest")
public class GetUsersTest extends ApiBaseTest {
    Parser parser;
    Users users;

    private static final Logger logger = LoggerFactory.getLogger(GetUsersTest.class);

    @Description("Get call test")
    @Link("http://project.jra.lt/browse/MPM-7777")
    @Test
    public void getUsersTest() throws IOException {

        logger.info("Test data");
        // empty

        logger.info("Initialize service");
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();
        users = new Users(requestContext);

        logger.info("Get users");
        APIResponse response = users.getUsers(GET_USERS);
        // parsing
        parser = new Parser(response.body());
        System.out.println(parser.responseBody().toPrettyString());

        logger.info("Verify user details");
        Map<String, String> headers = response.headers();
        Assert.assertEquals(headers.get("content-type"), "application/json; charset=utf-8");
        Assert.assertNotNull(parser.responseBody().get(0).get("id"));
        JsonArray json = new Gson().fromJson(response.text(), JsonArray.class);
        Assert.assertTrue(json.asList().stream().findFirst().toString().contains("minakshi_pandey@torp.test" ));
        for (JsonElement item : json) {
            JsonObject itemObj = item.getAsJsonObject();
            Assert.assertNotNull(itemObj.get("id"));
        }
        Assert.assertTrue(json.get(0).getAsJsonObject().get("email").getAsString().contains("minakshi_pandey@torp.test"));
    }
}