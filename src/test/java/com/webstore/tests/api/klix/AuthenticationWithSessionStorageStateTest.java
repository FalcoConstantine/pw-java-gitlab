package com.webstore.tests.api.klix;

import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.services.ConsumerCredit;
import com.webstore.api.services.Session;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import static com.webstore.pages.Constants.ApiPaths.*;
import static com.webstore.pages.Constants.Jsons.STATE;
import static com.webstore.pages.Constants.PersonalData.LV_LATEKO;

@Link("")
@Story("")
public class AuthenticationWithSessionStorageStateTest extends ApiBaseTest {

    Session session;
    ConsumerCredit consumerCredit;

    private Parser parser;
    private String personalId;
    private String id;
    private String sessionId;
    private String verificationCode;
    private String actualStatus;
    private String authSessionId;
    String expectedSessionStatus;
    String expectedAuthStatus;
    String sessionPath;
    String authPath;
    String cookiesPath;


    public static final Logger logger = LoggerFactory.getLogger(AuthenticationWithSessionStorageStateTest.class);


    @Description
    @Test
    public void authenticationWithSessionStorageStateTest() throws IOException {
        logger.info("Create test data");
        personalId = LV_LATEKO;
        id = "a68cdadb-2be2-4c49-98fa-0f65b7313aa3";
        expectedSessionStatus = "REQUESTED";
        expectedAuthStatus = "SUCCESSFUL";
        sessionPath = id + AUTH;


        logger.info("Initiate session services");
        session = new Session(requestContext);

        logger.info("Create new purchase and get session id");
        APIResponse response = session.createSession(sessionPath, personalId);
        parser = new Parser(response.body());
        sessionId = parser.responseBody().get("id").asText();
        verificationCode = parser.responseBody().get("verificationCode").asText();
        actualStatus = parser.responseBody().get("status").asText();

        System.out.println("Session ID -> " + sessionId);
        System.out.println("verificationCode: " + verificationCode);
        System.out.println("actualStatus: " + actualStatus);
        System.out.println("========= BODY ========================");
        System.out.println(parser.responseBody().toPrettyString());

        Assert.assertFalse(parser.responseBody().get("id").asText().isEmpty());
        Assert.assertFalse(parser.responseBody().get("verificationCode").asText().isEmpty());
        Assert.assertEquals(actualStatus, expectedSessionStatus);
        Map<String, String> cookies = response.headers();
        String token = cookies.get("set-cookie");
        System.out.println(cookies);
        System.out.println(token);


        logger.info("Get smart-id session response");
        authPath = sessionPath + "/" + sessionId;
        APIResponse smartIdResponse = session.getAuthResponse(authPath);
        parser = new Parser(smartIdResponse.body());
        authSessionId = parser.responseBody().get("id").asText();
        actualStatus = parser.responseBody().get("status").asText();
        System.out.println("Auth Session ID -> " + authSessionId);
        System.out.println("Actual Auth Status: " + actualStatus);
        System.out.println("========= BODY ========================");
        System.out.println(parser.responseBody().toPrettyString());

        Assert.assertFalse(parser.responseBody().get("id").asText().isEmpty());
        Assert.assertEquals(actualStatus, "REQUESTED");
        Assert.assertEquals(sessionId, authSessionId);


        logger.info("Get session");
        cookiesPath = authPath + SESSION;
        APIResponse cookieResponse = session.getCookies(cookiesPath);
        parser = new Parser(cookieResponse.body());
        String cookieSessionId = parser.responseBody().get("id").asText();
        actualStatus = parser.responseBody().get("status").asText();
        System.out.println("Cookies Session ID -> " + cookieSessionId);
        System.out.println("Actual Auth Status: " + actualStatus);
        System.out.println("========= BODY ========================");
        System.out.println(parser.responseBody().toPrettyString());

        Assert.assertFalse(parser.responseBody().get("id").asText().isEmpty());
        Assert.assertEquals(sessionId, cookieSessionId);
        Assert.assertEquals(actualStatus, "REQUESTED");
        String ckook = response.headers().get("set-cookie").split(";")[0];
        System.out.println(ckook);
//        Map<String, String> cookies1 = response.headers();
//        String token1 = cookies.get("set-cookie").split(";")[0];
//        System.out.println(cookies1);
//        System.out.println(token1);

        // Save storage state into the file.
        requestContext.storageState(new APIRequestContext.StorageStateOptions().setPath(Paths.get("state.json")));

        logger.info("Initiate Consumer-Credit services");
        // Create a new context with the saved storage state.
        requestContext = request.newContext(
                new APIRequest.NewContextOptions().setStorageStatePath(Paths.get("state.json")));
        consumerCredit = new ConsumerCredit(requestContext);

        // ISOLATION
//        // Create two isolated browser contexts
//        BrowserContext userContext = browser.newContext();
//        BrowserContext adminContext = browser.newContext();
//        // Create two isolated APIRequest contexts
//        APIRequestContext klixRequestContext = request.newContext();
//        APIRequestContext apiRequestContext = request.newContext();
//        klixRequestContext.post(url);
//        apiRequestContext.post(url);
//        // Create pages and interact with contexts independently

//         Load file and parse json file to receive values from attributes
        File file = new File(STATE);
        String SESSION = JsonPath.read(file, "$.cookies[0].value").toString();
        System.out.println("SESSION from state.json -> " + SESSION); // resultatas tas pats kaip it per .headers().get("set-cookie")

        // parcially disabled due to Klix sessions
//        logger.info("Get consumer credit details");
//        String path = prop.getProperty("klixShopApiBase");
////        APIResponse ccResponse = consumerCredit.getConsumerCredit( path + id + CONSUMER_CREDIT);
//        APIResponse ccResponse = consumerCredit.getConsumerCredit(path + id + CONSUMER_CREDIT, SESSION);
//
//        // Get request without service - straight forward request
////        Map<String, String> extraHeaders
////                = new HashMap<>();
////        extraHeaders.put("Accept", "application/json");
////        extraHeaders.put("Content-Type", "application/json");
////        extraHeaders.put("Cookie", "SESSION=" + SESSION);
////        APIResponse ccResponse = request.newContext(new APIRequest.NewContextOptions()
////                .setBaseURL(prop.getProperty("klixShopApiBase"))
////                .setExtraHTTPHeaders(extraHeaders)).get(id + CONSUMER_CREDIT);
////        System.out.println(ccResponse.text());
////        System.out.println(ccResponse.url());
////        Assert.assertEquals(ccResponse.status(), OK);
//
//        parser = new Parser(ccResponse.body());
//        String customerId = parser.responseBody().get("customerId").asText();
//        String orderId = parser.responseBody().get("orderId").asText();
//        actualStatus = parser.responseBody().get("status").asText();
//        System.out.println("Customer ID -> " + customerId);
//        System.out.println("Order id: " + orderId);
//        System.out.println("========= BODY ========================");
//        System.out.println(parser.responseBody().toPrettyString());
//
//        Assert.assertFalse(parser.responseBody().get("id").asText().isEmpty());
//        Assert.assertEquals(actualStatus, "CUSTOMER_RISK_EVALUATION_SUCCEEDED");
    }
}
