package com.webstore.tests.api.klix.negative;

import com.microsoft.playwright.APIResponse;
import com.webstore.api.services.ConsumerCredit;
import com.webstore.api.services.Exceptions;
import com.webstore.api.services.Purchases;
import com.webstore.api.services.Session;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.webstore.pages.Constants.ApiPaths.*;
import static com.webstore.pages.Constants.PersonalData.LV_LATEKO;

@Link("https://jira.project.MPM-7777")
@Story("Order expired negative case")
public class OrderExpiredNegativeTest extends ApiBaseTest {

    Exceptions exceptions;

    private Parser parser;
    private String personalId;
    private String id;
    String expectedAuthStatus;
    String sessionPath;

    public static final Logger logger = LoggerFactory.getLogger(OrderExpiredNegativeTest.class);

    @Description("Order expired negative test case")
    @Test
    public void orderExpiredNegativeTest() throws IOException {
        logger.info("Create test data");
        personalId = LV_LATEKO;
        id = "0a673858-01fa-45df-b12e-3a1e8de11aff";
        String expectedErrorCode = "ORDER_EXPIRED";
        String expectedMessage = "The payment period for this order has expired, please return to the merchant's " +
                "store and place the order again";
        expectedAuthStatus = "SUCCESSFUL";
        sessionPath = id + AUTH;

        logger.info("Initiate services");
        exceptions = new Exceptions(requestContext);

        logger.info("Create new purchase and get session id");
        APIResponse response = exceptions.orderExpired(sessionPath, personalId);
        parser = new Parser(response.body());
        String errorId = parser.responseBody().get("errorId").asText();
        String timestamp = parser.responseBody().get("timestamp").asText();
        String errorCode = parser.responseBody().get("errorCode").asText();
        String message = parser.responseBody().get("errors").get(0).get("message").asText();
        String code = parser.responseBody().get("errors").get(0).get("code").asText();
        String retryable = parser.responseBody().get("errors").get(0).get("retryable").asText();
        String language = parser.responseBody().get("language").asText();

        System.out.println("========= BODY ========================");
        System.out.println(parser.responseBody().toPrettyString());

        logger.info("Verify response body attributes");
        Assert.assertEquals(code, expectedErrorCode);
        Assert.assertEquals(message, expectedMessage);
    }
}
