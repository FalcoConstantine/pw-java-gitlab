package com.webstore.tests.api.paymets;

import com.github.javafaker.Faker;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.webstore.api.dao.dto.ClientRequestDTO;
import com.webstore.api.dao.dto.ProductRequestDTO;
import com.webstore.api.dao.dto.PurchaseRequestDTO;
import com.webstore.api.dao.request.purchase.PurchaseRequest;
import com.webstore.api.dao.response.purchase.PurchaseResponse;
import com.webstore.api.services.Purchases;
import com.webstore.tests.ApiBaseTest;
import com.webstore.utils.Parser;
import com.webstore.utils.Randomiser;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import static com.webstore.pages.Constants.ApiPaths.PURCHASES;
import static com.webstore.pages.Constants.Brands.TEST_BRAND_ID;
import static com.webstore.pages.Constants.Jsons.MASTERCARD;
import static com.webstore.pages.Constants.Users.TEST_AUTOMATION_EMAIL;

@Story("Create Purchase Positive Test")
public class CreatePurchasePositiveTest2 extends ApiBaseTest {
    //services
    Purchases purchases;

    // variables
    private String product;
    private Integer price;
    private String cardNumber;
    private String cardName;
    private String expDate;
    private String cvv2;
    boolean forceRecurring;

    // utils
    private Parser parser;
    private String email;
    private JsonArray json;
    File file;
    static Random random;
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);

    SoftAssert softAssert;

    public static final Logger logger = LoggerFactory.getLogger(CreatePurchasePositiveTest2.class);

    @Step("Create purchase positive test with product: {1} and price: {2}")
    @Test
    public void createPurchasePositiveTest() throws IOException {

        softAssert = new SoftAssert();

        logger.info("Create test data");
        product = randomiser.getRandomString().trim();
        price = 110000;
        email = TEST_AUTOMATION_EMAIL;
        forceRecurring = false;

        // Load file and parse json file to receive values from attributes
        file = new File(MASTERCARD);
        cardNumber = JsonPath.read(file, "$.PAN").toString();
        cardName = JsonPath.read(file, "$.issuer").toString();
        expDate = JsonPath.read(file, "$.expiry_date").toString();
        cvv2 = JsonPath.read(file, "$.CVV2").toString();

        //Creates new Object via Builder
        PurchaseRequest payloadBody = PurchaseRequest.builder()
                .client(new ClientRequestDTO(email))
                .purchase(new PurchaseRequestDTO("en",
                        new ArrayList<>(Collections.singleton(
                                new ProductRequestDTO(product, price)))
                ))
                .brand_id(TEST_BRAND_ID)
                .skip_capture(false)
                .build();

        logger.info("Initiate services");
        purchases = new Purchases(requestContext);

        logger.info("Create purchase");
        // Upload updated json object
//        APIResponse response = purchases.create(PURCHASES, product, price, email, forceRecurring);

        // Uploads new Object
        PurchaseResponse response = purchases.create(PURCHASES, payloadBody);

//        parser = new Parser(response.body());
//        String id = parser.responseBody().get("id").asText();
//        String checkout_url = parser.responseBody().get("checkout_url").asText();
//        System.out.println("CreatePurchasePositiveTest ID -> " + id);
//        System.out.println("Checkout URL: " + checkout_url);
//        System.out.println("========= BODY ========================");
//        System.out.println(parser.responseBody().toPrettyString());

        // Solution 1
//        Assert.assertTrue(response.text().contains(name));
//        Assert.assertTrue(response.text().contains(email));
        // Solution 2
//        Assert.assertEquals(parser.responseBody().get("name").asText(), name);
//        Assert.assertEquals(parser.responseBody().get("email").asText(), email);

        // Solution 3
//        logger.info("Get UsersRequest list and verify if user has been created");
//        APIResponse responseBody = users.getUsers(GET_USERS);
//        System.out.println("GET users response: " + responseBody.text());
//        System.out.println("GET URL AFTER CREATION: " + responseBody.url());
//        JsonArray json = new Gson().fromJson(responseBody.text(), JsonArray.class);
//        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(name));
//        Assert.assertTrue(json.asList().stream().findFirst().toString().contains(email));
//
        // Solution 4
//        logger.info("Get ID of created user");
//        parser = new Parser(responseBody.body());
//        String userId = parser.responseBody().get(0).get("id").toString();
////        .asList().stream().findFirst().toString().trim();
//        System.out.println("User ID " + userId);

        logger.info("Verify purchase response attributes");
        softAssert.assertNotNull(response.getId());
        softAssert.assertTrue(response.getCheckoutUrl().contains("https://portal.klix.app/p"));
        softAssert.assertNull(response.getClient().getClientType());
        softAssert.assertEquals(response.getClient().getEmail(), email);
        softAssert.assertEquals(response.getClient().getPhone(), "");
        softAssert.assertEquals(response.getClient().getFullName(), "");
        softAssert.assertEquals(response.getClient().getPersonalCode(), "");
        softAssert.assertEquals(response.getClient().getLegalName(), "");
        softAssert.assertEquals(response.getClient().getBrandName(), "");
        softAssert.assertEquals(response.getClient().getRegistrationNumber(), "");
        softAssert.assertEquals(response.getClient().getTaxNumber(), "");
        softAssert.assertEquals(response.getClient().getBankAccount(), "");
        softAssert.assertEquals(response.getClient().getBankCode(), "");
        softAssert.assertEquals(response.getClient().getStreetAddress(), "");
        softAssert.assertEquals(response.getClient().getCity(), "");
        softAssert.assertEquals(response.getClient().getZipCode(), "");
        softAssert.assertEquals(response.getClient().getCountry(), "");
        softAssert.assertEquals(response.getClient().getState(), "");
        softAssert.assertEquals(response.getClient().getShippingStreetAddress(), "");
        softAssert.assertEquals(response.getClient().getShippingCity(), "");
        softAssert.assertEquals(response.getClient().getShippingZipCode(), "");
        softAssert.assertEquals(response.getClient().getShippingCountry(), "");
        softAssert.assertEquals(response.getClient().getShippingState(), "");
        softAssert.assertEquals(response.getClient().getDeliveryMethods().get(0).getMethod(), "email");
        softAssert.assertEquals(response.getPurchase().getCurrency(), "EUR");
        softAssert.assertEquals(response.getPurchase().getProducts().get(0).getName(), product);
        softAssert.assertEquals(response.getPurchase().getProducts().get(0).getPrice(), price);
        softAssert.assertEquals(response.getPurchase().getProducts().get(0).getQuantity(), "1.0000");
        softAssert.assertEquals(response.getPurchase().getProducts().get(0).getTaxPercent(), "0.00");
        softAssert.assertEquals(response.getPurchase().getProducts().get(0).getCategory(), "");
        softAssert.assertEquals(response.getPurchase().getLanguage(), "en");
        softAssert.assertEquals(response.getPurchase().getDebt(), 0);

        softAssert.assertAll();

//subtotal_override=null, total_tax_override=null, total_discount_override=null, total_override=null, total=110000, request_client_details=[], timezone=Europe/Riga, due_strict=false, email_message=, shipping_options=[], payment_method_details=PaymentMethodDetailsDTO(), has_upsell_products=false), payment=null, issuerDetails=IssuerDetailsDTO(brand_name=KLIX TEST MERCHANT, website=, legal_name=KLIX TEST MERCHANT, registration_number=40277777777, tax_number=, legal_street_address=Tirgotāja adrese, legal_country=LV, legal_city=Riga, legal_zip_code=LV1010, bank_accounts=[BankAccountDTO(bank_account=LV38PARX002252026000777, bank_code=PARXLV22)]), transactionData=TransactionDataDTO(payment_method=, flow=payform, extra=ExtraDTO(), country=, attempts=[], card_region=), status=created, statusHistory=[StatusHistoryDTO(status=created, timestamp=1702380060)], viewedOn=null, forceRecurring=false, companyId=6a5056be-e07d-4c96-b68b-34b4d5dad090, isTest=false, userId=null, brandId=702314b8-dd86-41fa-9a22-510fdd71fa92, billingTemplateId=null, orderId=null, clientId=b9e151c2-7ec6-48e7-9d78-e7d9932cdb6d, sendReceipt=false, isRecurringToken=false, recurringToken=null, skipCapture=false, referenceGenerated=KL117645, reference=, issued=2023-12-12, due=1702383660, refundAvailability=none, refundableAmount=0, currencyConversion=null, paymentMethodWhitelist=null, successRedirect=, failureRedirect=, cancelRedirect=, successCallback=, markedAsPaid=false, upsellCampaigns=[], referralCampaignId=null, referralCode=null, referralCodeDetails=null, referralCodeGenerated=null, retainLevelDetails=null, canRetrieve=false, canChargeback=false, creatorAgent=, platform=api, product=purchases, createdFromIp=146.70.89.201, invoiceUrl=null, checkoutUrl=https://portal.klix.app/p/acc1fc2b-c476-4458-8078-15e21873f5fe/, directPostUrl=null, createdOn=1702380060, updatedOn=1702380060, type=purchase, id=acc1fc2b-c476-4458-8078-15e21873f5fe,

    }
}
