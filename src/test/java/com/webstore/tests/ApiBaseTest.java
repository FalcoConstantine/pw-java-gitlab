package com.webstore.tests;

import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.Playwright;
import com.webstore.configuration.ApiFactory;
import com.webstore.pages.Pages;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ApiBaseTest extends Pages {

    protected ApiFactory apiFactory;
    protected Playwright playwright;
    protected APIRequest request;
    protected APIRequestContext requestContext;

    @BeforeClass
    public void setup() throws IOException {
        apiFactory = new ApiFactory(requestContext);
        prop = apiFactory.init_prop();

        // Bearer
        Map<String, String> extraHeaders
                = new HashMap<>();
        extraHeaders.put("Accept", "application/json");
        extraHeaders.put("Content-Type", "application/json");
        extraHeaders.put("Authorization", prop.getProperty("token"));
        playwright = Playwright.create();
        request = playwright.request();
        requestContext = request.newContext(new APIRequest.NewContextOptions()
                .setBaseURL(prop.getProperty("cardsApiBase"))
                .setExtraHTTPHeaders(extraHeaders));

        // Session
        //TODO - develop session authentication here
    }

    @AfterClass
    public void tearDown() {
        playwright.close();
    }
}
