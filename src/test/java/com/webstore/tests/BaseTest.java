package com.webstore.tests;

import com.microsoft.playwright.Page;
import com.webstore.configuration.BrowserFactory;
import com.webstore.pages.Pages;
import com.webstore.pages.StartTest;
import org.testng.annotations.*;

import java.io.IOException;

public class BaseTest extends Pages {

    protected BrowserFactory config;
    Page page;

    @Parameters({"browser"})
    @BeforeClass
    public void setup(String browser) throws IOException {
            config = new BrowserFactory();
            prop = config.init_prop();

            if (browser != null) {
                prop.setProperty("browser", browser);
            }
            page = config.initBrowser(prop);
            startTest = new StartTest(page);
        }

    @AfterClass
    public void tearDown() {
        page.context().browser().close();
    }
}