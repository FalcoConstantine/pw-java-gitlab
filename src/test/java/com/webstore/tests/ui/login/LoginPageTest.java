package com.webstore.tests.ui.login;

import com.webstore.tests.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.Assert;

import static com.webstore.pages.Constants.LoginPage.TITLE_LOGIN_PAGE;

@Story("Verify that customer is able to login")
public class LoginPageTest extends BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(LoginPageTest.class);

    @Link("https://jira.project.lt/browse/PR-5731")
    @Description("Navigation to the Login page")
    @Test()
    public void appLoginTest() {
        logger.info("Verify that customer is able navigate to the login page");
        loginPage = homePage.navigateToLoginPage();

        logger.info("Get the title of the Login page");
        String title  = loginPage.getLoginPageTitle();
        logger.info("Get status of the \'Forgotten Password\' link on the Login form");
        boolean isLinkVisible = loginPage.isForgotPasswordLinkVisible();

        logger.info("Verify that customer is able to login with credentials");
        accountPage = loginPage.fillEmailFiled(prop.getProperty("username"))
                .fillPasswordFiled(prop.getProperty("password"))
                .clickLoginButton();
        accountPage.test1().test2().test3();

        // Assertions
        logger.info("Verify the title of the Login page is correct");
        Assert.assertEquals(title, TITLE_LOGIN_PAGE);
        logger.info("Verify is \'Forgotten Password\' link visible on Login form");
        Assert.assertTrue(isLinkVisible);
    }
}
