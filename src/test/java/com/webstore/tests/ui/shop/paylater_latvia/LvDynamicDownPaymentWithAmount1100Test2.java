package com.webstore.tests.ui.shop.paylater_latvia;

import com.webstore.tests.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static com.webstore.pages.Constants.Address.LV_ADDRESS;
import static com.webstore.pages.Constants.Email.LV_EMAIL;
import static com.webstore.pages.Constants.Employers.LV_EMPLOYER;
import static com.webstore.pages.Constants.Items.AMOUNT_1100;
import static com.webstore.pages.Constants.PaymentDates.DATE_5;
import static com.webstore.pages.Constants.PersonalData.LV_LATEKO;
import static com.webstore.pages.Constants.Phone.LV_PHONE;
import static com.webstore.pages.Localisation.IncomeSource.SALARY_EN;
import static com.webstore.pages.Localisation.MaritalStatus.MARRIED_EN;
import static com.webstore.pages.Localisation.NumberOfDependents.NULL;

public class LvDynamicDownPaymentWithAmount1100Test2 extends BaseTest {

    boolean isLogoElementVisible;
    boolean isTitleVisible;
    boolean isPhoneVisible;
    boolean isEmailsAddressVisible;
    boolean isDownPaymentFieldAvailable;
    boolean isDownPaymentAmountBannerVisibleOnOffersPage;
    boolean isDownPaymentAmountBannerVisibleOnAgreementPage;
    String actualPhoneNumber;
    String actualEmailAddress;

    private static final Logger logger = LoggerFactory.getLogger(LvDynamicDownPaymentWithAmount1100Test2.class);

    //TODO - fixme
    //FIXME - Add more fields:
    // - more fitures

    @Link("https://jira.project.lt/browse/PR-5718")
    @Description("Add item to cart, proceed with checkout select LV method, authenticate with Demo Smart-ID and fill the application")
    @Test
    public void LvDynamicDownPaymentWithAmount1100Test() {
        logger.info("Test data");
        String itemAmount = AMOUNT_1100;
        String personalId = LV_LATEKO;
        String phone = LV_PHONE;
        String email = LV_EMAIL;
        String address = LV_ADDRESS;
        String maritalStatus = MARRIED_EN;
        String dependents = NULL;
        String incomeSource = SALARY_EN;
        String employerName = LV_EMPLOYER;
        String day = DATE_5;
        String income = "1600";
        String credit = "0";
        String downPayment = "110";

        logger.info("Open Shop page");
        shopPage = startTest.onUatShopPage();
        logger.info("Add item to cart");
        cartPage = shopPage.closeBanner()
                .addItemToCartByPrice(itemAmount)
                .openCart();
//        logger.info("Proceed with checkout");
//        paymentMethodPage = cartPage.pressCheckoutButton()
//                .closeBanner()
//                .selectGuestCheckoutMethod()
//                .pressOnChoosePaymentMethodButton();
//        logger.info("Select payment method");
//        authenticationPage = paymentMethodPage.pressLvPayLaterPaymentMethodButton();
//        applicationFormPage = authenticationPage.selectDemoSmartIdAuth()
//                .fillCustomerPersonalCode(personalId)
//                .pressContinueButton();
//        isDownPaymentFieldAvailable = applicationFormPage.getDownPaymentField();
//        dataCorrectPage = applicationFormPage
//                .fillPhoneNumber(phone)
//                .fillEmailAddress(email)
//                .selectAddress(address)
//                .selectMaritalStatus(maritalStatus)
//                .selectNumberOfDependents(dependents)
//                .selectIncomeType(incomeSource)
//                .selectEmployerName(employerName)
//                .fillMonthlyIncome(income)
//                .fillCreditLiabilities(credit)
//                .selectPaymentDay(day)
//                .fillDownPaymentAmount(downPayment)
//                .provideAgreementFullData()
//                .provideAgreementShareData()
//                .pressContinueButton();
//        isLogoElementVisible = dataCorrectPage.isLogoVisible();
//        isPhoneVisible = dataCorrectPage.isPhoneVisible();
//        isEmailsAddressVisible = dataCorrectPage.isEmailVisible();
//        isTitleVisible = dataCorrectPage.isTitleVisible();
//        actualPhoneNumber = dataCorrectPage.getPhoneNumber();
//        actualEmailAddress = dataCorrectPage.getEmailAddress().trim();
//        offersPage = dataCorrectPage.pressYesAllGoodButton(ENG);
//        isDownPaymentAmountBannerVisibleOnOffersPage = offersPage.isDownPaymentBannerVisible();
//        agreementPage = offersPage.pressAgreementButton(ENG);
//        isDownPaymentAmountBannerVisibleOnAgreementPage = agreementPage.isDownPaymentBannerVisible();
//        completePaymentPage = agreementPage.pressAgreementConsent()
//                .signTheAgreement(ENG);
//
////        logger.info("Verify that down payment amount field is available");
////        Assert.assertTrue(isDownPaymentFieldAvailable);
//        logger.info("Verify that Logo \'Klix\' is on the page");
//        Assert.assertTrue(isLogoElementVisible, "Logo is visible on the page");
//        logger.info("Verify that Title is on the page");
//        Assert.assertTrue(isTitleVisible);
//        logger.info("Verify that Phone number is on the page");
//        Assert.assertTrue(isPhoneVisible);
//        logger.info("Verify that Email address is on the page");
//        Assert.assertTrue(isEmailsAddressVisible);
//        logger.info("Verify that phone and email are correct");
//        Assert.assertEquals(actualPhoneNumber, "+371" +  phone);
//        Assert.assertEquals(actualEmailAddress, email);
//        Assert.assertTrue(isDownPaymentAmountBannerVisibleOnOffersPage, "Down payment banner is visible on offers page");
//        logger.info("Verify that down payment banner is visible in agreement page");
//        Assert.assertTrue(isDownPaymentAmountBannerVisibleOnAgreementPage, "Down payment banner is visible on agreement page");
    }
}