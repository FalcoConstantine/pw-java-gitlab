package com.webstore.tests.ui.shop.paylater_lithuania;

import com.webstore.tests.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Link;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static com.webstore.pages.Constants.Address.LT_ADDRESS;
import static com.webstore.pages.Constants.PersonalData.LT_CITADELE;
import static com.webstore.pages.Localisation.AuthenticationPage.*;
import static com.webstore.pages.Locators.ShopPageLocators.ITEM_200;

public class PayLaterLithuaniaTest extends BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(PayLaterLithuaniaTest.class);

    @Link("https://jira.project.lt/browse/PR-5719")
    @Description("Add 200 Eu item to cart, proceed with checkout select LT method, authenticate with Demo Smart-ID and fill the application")
    @Test
    public void fillLatvianPayLaterApplicationTest() {
        logger.info("Test data");
        String personalId = LT_CITADELE;
        String address = LT_ADDRESS;
        String amount = "2000";
        String credit = "200";

//        logger.info("Add item to cart");
//        cartPage = shopPage.closeBanner()
//                .addItemToCart(ITEM_200)
//                .openCart();
//        logger.info("Proceed with checkout");
//        paymentMethodPage = cartPage.proceedWithCheckout()
//                .chooseCustomer()
//                .choosePaymentMethod();
//        logger.info("Select payment method");
//        authenticationPage = paymentMethodPage.selectLithuanianPayLaterPaymentMethod();
//        applicationFormPage = authenticationPage.selectDemoSmartIdAuth(MOCK_SMART_ID_LT)
//                .enterPersonalId(personalId)
//                .pressContinueButton(CONTINUE_BTN_LT);
//        applicationFormPage.selectAddress(address)
//                .chooseMaritalStatus(MARRIED)
//                .setMonthlyIncome(amount)
//                .setCreditLiabilitiesInLT(credit)
//                .setDownPaymentAmount()
//                .provideConsentInLT();
    }
}