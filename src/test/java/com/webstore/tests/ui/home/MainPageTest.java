package com.webstore.tests.ui.home;

import com.webstore.tests.BaseTest;
import io.qameta.allure.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.webstore.pages.Localisation.MainPage.SEARCH_HEADER_TEXT_SEARCH;

@Story("Verify filtering by product name")
public class MainPageTest extends BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(MainPageTest.class);

    @DataProvider
    public Object[][] getProductData() {
        return new Object[][]{
                {"Macbook"},
                {"iMac"},
                {"Samsung"}
        };
    }

    @Link("https://jira.project.lt/browse/PR-5718")
    @Description("Verify that after providing product: '{1}' the header is visible and text is correct")
    @Test(dataProvider = "getProductData")
    public void searchTest(String product) {
        logger.info("Get header text after product filtering");
        String actualHeaderText = homePage.doSearch(product).getTextOfHeader();
        logger.info("Verify that header text is correct");
        Assert.assertEquals(actualHeaderText, SEARCH_HEADER_TEXT_SEARCH + product);
    }
}