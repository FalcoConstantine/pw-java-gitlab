package com.webstore.utils;

import com.github.javafaker.CreditCardType;
import com.github.javafaker.Faker;
import lombok.Data;

import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Data
public class Randomiser {

    private Random random;
    private Faker faker;

    public Randomiser(Faker faker, Random random) {
        this.faker = faker;
        this.random = random;
    }

//    public String getRandomCity() {
//        return faker.address().city();
//    }

//    public void getRandomAddress() {
//        String city = faker.address().city();
//        String state = faker.address().state();
//        String country = faker.address().country();
//        String streetName = faker.address().streetName();
//        String streetAddrss = faker.address().streetAddress();
//        String zipCode = faker.address().zipCode();
//    }

//    public void financing() {
//        faker.finance().creditCard(CreditCardType.AMERICAN_EXPRESS);
//        faker.finance().creditCard(CreditCardType.VISA);
//        faker.finance().creditCard(CreditCardType.MASTERCARD);
//    }

    public String getRandomEmail() {
        return faker.internet().emailAddress();
    }

    public String getRandomName() {
        return faker.name().fullName();
    }

    public String getRandomString() {
        // source: https://github.com/DiUS/java-faker/issues/299
        return faker.lorem().fixedString(5);
    }

//    public void credentials() {
//        faker.phoneNumber().cellPhone();
//        faker.phoneNumber().phoneNumber();
//        faker.name().prefix();
//        faker.name().firstName();
//        faker.name().lastName();
//        faker.name().suffix();
//        faker.name().fullName();
//        faker.name().nameWithMiddle();
//        faker.name().username();
//    }
//
//    public void dataAndTime(Date from, Date to) {
//        faker.date().between(from, to);
//    }
//
//    public void futureDate(int atMost, TimeUnit unit) {
////        faker.date().future (atMost, unit.toDays());
//    }
//
//    public Integer randomNumberBetweenMinMax(Integer min, Integer max) {
//        return faker.random().nextInt(min, max);
//    }


}
