package com.webstore.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.playwright.APIResponse;
import com.webstore.api.dao.response.UsersResponse;

import java.io.IOException;
import java.io.InputStream;

public class Parser {

    private byte[] response;

    public Parser(byte[] response) {
        this.response = response;
    }

    public Parser() {

    }

    public JsonNode responseBody() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response);
    }

    public Object responseBody(APIResponse response) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(response.text(), Object.class);
    }

}
