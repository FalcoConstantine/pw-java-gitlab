package com.webstore.api.dao.response.purchase;

import com.fasterxml.jackson.annotation.*;
import com.webstore.api.dao.dto.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// source: https://www.jsonschema2pojo.org/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "client",
        "purchase",
        "payment",
        "issuer_details",
        "transaction_data",
        "status",
        "status_history",
        "viewed_on",
        "force_recurring",
        "company_id",
        "is_test",
        "user_id",
        "brand_id",
        "billing_template_id",
        "order_id",
        "client_id",
        "send_receipt",
        "is_recurring_token",
        "recurring_token",
        "skip_capture",
        "reference_generated",
        "reference",
        "issued",
        "due",
        "refund_availability",
        "refundable_amount",
        "currency_conversion",
        "payment_method_whitelist",
        "success_redirect",
        "failure_redirect",
        "cancel_redirect",
        "success_callback",
        "marked_as_paid",
        "upsell_campaigns",
        "referral_campaign_id",
        "referral_code",
        "referral_code_details",
        "referral_code_generated",
        "retain_level_details",
        "can_retrieve",
        "can_chargeback",
        "creator_agent",
        "platform",
        "product",
        "created_from_ip",
        "invoice_url",
        "checkout_url",
        "direct_post_url",
        "created_on",
        "updated_on",
        "type",
        "id"
})
public class PurchaseResponse {

    @JsonProperty("client")
    private ClientResponseDTO client;
    @JsonProperty("purchase")
    private PurchaseResponseDTO purchase;
    @JsonProperty("payment")
    private Object payment;
    @JsonProperty("issuer_details")
    private IssuerDetailsDTO issuerDetails;
    @JsonProperty("transaction_data")
    private TransactionDataDTO transactionData;
    @JsonProperty("status")
    private String status;
    @JsonProperty("status_history")
    private List<StatusHistoryDTO> statusHistory;
    @JsonProperty("viewed_on")
    private Object viewedOn;
    @JsonProperty("force_recurring")
    private Boolean forceRecurring;
    @JsonProperty("company_id")
    private String companyId;
    @JsonProperty("is_test")
    private Boolean isTest;
    @JsonProperty("user_id")
    private Object userId;
    @JsonProperty("brand_id")
    private String brandId;
    @JsonProperty("billing_template_id")
    private Object billingTemplateId;
    @JsonProperty("order_id")
    private Object orderId;
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("send_receipt")
    private Boolean sendReceipt;
    @JsonProperty("is_recurring_token")
    private Boolean isRecurringToken;
    @JsonProperty("recurring_token")
    private Object recurringToken;
    @JsonProperty("skip_capture")
    private Boolean skipCapture;
    @JsonProperty("reference_generated")
    private String referenceGenerated;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("issued")
    private String issued;
    @JsonProperty("due")
    private Integer due;
    @JsonProperty("refund_availability")
    private String refundAvailability;
    @JsonProperty("refundable_amount")
    private Integer refundableAmount;
    @JsonProperty("currency_conversion")
    private Object currencyConversion;
    @JsonProperty("payment_method_whitelist")
    private Object paymentMethodWhitelist;
    @JsonProperty("success_redirect")
    private String successRedirect;
    @JsonProperty("failure_redirect")
    private String failureRedirect;
    @JsonProperty("cancel_redirect")
    private String cancelRedirect;
    @JsonProperty("success_callback")
    private String successCallback;
    @JsonProperty("marked_as_paid")
    private Boolean markedAsPaid;
    @JsonProperty("upsell_campaigns")
    private List<Object> upsellCampaigns;
    @JsonProperty("referral_campaign_id")
    private Object referralCampaignId;
    @JsonProperty("referral_code")
    private Object referralCode;
    @JsonProperty("referral_code_details")
    private Object referralCodeDetails;
    @JsonProperty("referral_code_generated")
    private Object referralCodeGenerated;
    @JsonProperty("retain_level_details")
    private Object retainLevelDetails;
    @JsonProperty("can_retrieve")
    private Boolean canRetrieve;
    @JsonProperty("can_chargeback")
    private Boolean canChargeback;
    @JsonProperty("creator_agent")
    private String creatorAgent;
    @JsonProperty("platform")
    private String platform;
    @JsonProperty("product")
    private String product;
    @JsonProperty("created_from_ip")
    private String createdFromIp;
    @JsonProperty("invoice_url")
    private Object invoiceUrl;
    @JsonProperty("checkout_url")
    private String checkoutUrl;
    @JsonProperty("direct_post_url")
    private Object directPostUrl;
    @JsonProperty("created_on")
    private Integer createdOn;
    @JsonProperty("updated_on")
    private Integer updatedOn;
    @JsonProperty("type")
    private String type;
    @JsonProperty("id")
    private String id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();
}