package com.webstore.api.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
//@NoArgsConstructor
public class ClientRequestDTO {
    private String email;
}
