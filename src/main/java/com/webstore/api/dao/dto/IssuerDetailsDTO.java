package com.webstore.api.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssuerDetailsDTO {
    private String brand_name;
    private String website;
    private String legal_name;
    private String registration_number;
    private String tax_number;
    private String legal_street_address;
    private String legal_country;
    private String legal_city;
    private String legal_zip_code;
    private ArrayList<BankAccountDTO> bank_accounts;
}
