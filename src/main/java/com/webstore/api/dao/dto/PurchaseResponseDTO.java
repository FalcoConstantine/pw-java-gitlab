package com.webstore.api.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponseDTO {
    private String currency;
    private ArrayList<ProductResponseDTO> products;
    private String language;
    private String notes;
    private int debt;
    private Object subtotal_override;
    private Object total_tax_override;
    private Object total_discount_override;
    private Object total_override;
    private int total;
    private Object request_client_details;
    private String timezone;
    private boolean due_strict;
    private String email_message;
    private Object shipping_options;
    private PaymentMethodDetailsDTO payment_method_details;
    private boolean has_upsell_products;
}