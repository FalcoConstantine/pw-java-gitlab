package com.webstore.api.dao.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "client_type",
        "email",
        "phone",
        "full_name",
        "personal_code",
        "legal_name",
        "brand_name",
        "registration_number",
        "tax_number",
        "bank_account",
        "bank_code",
        "street_address",
        "city",
        "zip_code",
        "country",
        "state",
        "shipping_street_address",
        "shipping_city",
        "shipping_zip_code",
        "shipping_country",
        "shipping_state",
        "cc",
        "bcc",
        "delivery_methods"
})
public class ClientResponseDTO {
    @JsonProperty("client_type")
    private Object clientType;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("personal_code")
    private String personalCode;
    @JsonProperty("legal_name")
    private String legalName;
    @JsonProperty("brand_name")
    private String brandName;
    @JsonProperty("registration_number")
    private String registrationNumber;
    @JsonProperty("tax_number")
    private String taxNumber;
    @JsonProperty("bank_account")
    private String bankAccount;
    @JsonProperty("bank_code")
    private String bankCode;
    @JsonProperty("street_address")
    private String streetAddress;
    @JsonProperty("city")
    private String city;
    @JsonProperty("zip_code")
    private String zipCode;
    @JsonProperty("country")
    private String country;
    @JsonProperty("state")
    private String state;
    @JsonProperty("shipping_street_address")
    private String shippingStreetAddress;
    @JsonProperty("shipping_city")
    private String shippingCity;
    @JsonProperty("shipping_zip_code")
    private String shippingZipCode;
    @JsonProperty("shipping_country")
    private String shippingCountry;
    @JsonProperty("shipping_state")
    private String shippingState;
    @JsonProperty("cc")
    private List<Object> cc;
    @JsonProperty("bcc")
    private List<Object> bcc;
    @JsonProperty("delivery_methods")
    private List<DeliveryMethodDTO> deliveryMethods;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();
}
