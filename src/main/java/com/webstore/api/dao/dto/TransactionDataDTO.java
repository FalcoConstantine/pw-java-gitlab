package com.webstore.api.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDataDTO {
    private String payment_method;
    private String flow;
    private ExtraDTO extra;
    private String country;
    private Object attempts;
    private String card_region;
}
