package com.webstore.api.dao.request.purchase;

import com.webstore.api.dao.dto.ClientRequestDTO;
import com.webstore.api.dao.dto.PurchaseRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PurchaseRequest {

    private ClientRequestDTO client;
    private PurchaseRequestDTO purchase;
    private String brand_id;
    private boolean skip_capture;


}
