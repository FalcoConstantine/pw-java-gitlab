package com.webstore.api.services;

import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import org.testng.Assert;

import static com.webstore.pages.Constants.HttpStatus.OK;

public class ConsumerCredit {

    private APIRequestContext requestContext;

    public ConsumerCredit(APIRequestContext requestContext) {
        this.requestContext = requestContext;
    }

    public APIResponse getConsumerCredit(String url) {
        APIResponse response = requestContext.get(url);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return response;
    }

    public APIResponse getConsumerCredit(String url, String token) {
        APIResponse response = requestContext.get(url, RequestOptions.create().setHeader("Cookie", "SESSION=" + token));
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return response;
    }
}