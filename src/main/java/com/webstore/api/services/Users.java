package com.webstore.api.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import com.webstore.api.dao.response.UsersResponse;
import com.webstore.utils.Parser;
import io.qameta.allure.Step;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.webstore.pages.Constants.ApiPaths.GET_USERS;
import static com.webstore.pages.Constants.HttpStatus.*;
import static com.webstore.pages.Constants.Jsons.USERS_JSON;

public class Users {

    private APIRequestContext requestContext;

    public Users(APIRequestContext requestContext) {
        this.requestContext = requestContext;
    }

    ObjectMapper objectMapper;
    UsersResponse user;
    UsersResponse userBody;


    @Step("Get list of users")
    public APIResponse getUsers(String url) {
        APIResponse response = requestContext.get(url);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return response;
        //requestContext.get(url);
    }

    @Step("Get user details by ID: {1} and Name: {2}")
    public APIResponse getUserByIdAndName(String url, String id, String name) {
        APIResponse response =requestContext.get(GET_USERS, RequestOptions.create()
                        .setQueryParam("id", id)
                        .setQueryParam("name", name)
        );
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return response;
    }

    public APIResponse createUser(String url, String name, String email) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", name);
        data.put("email", email);
        data.put("gender", "male");
        data.put("status", "active");
        APIResponse response = requestContext.post(url, RequestOptions.create()
                .setData(data));
        System.out.println(response.text());
        System.out.println(response.url());
        return response;
    }

    // Update Json payload body
    @Step("Create user with random name: {1} and email: {2}")
    public APIResponse createUser(String url, String name, String email, String gender, String status) throws IOException {
        File file = new File(USERS_JSON);
        String jsonPayload = JsonPath.parse(file)
                .set("$.name", name)
                .set("$.email", email)
                .set("$.gender", gender)
                .set("$.status", status)
                .jsonString();
        APIResponse response = requestContext.post(url, RequestOptions.create()
                .setData(jsonPayload));
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), CREATED);
        return response;
    }

    @Step("Create user with random name: {1} and email: {2} via POJO")
    public UsersResponse createUserWithPojo(String url, Object user) throws IOException {
    // Given object with params
//    public UsersResponse createUserWithPojo(String url, String name, String email, String gender, String status) throws IOException {
//        user = new UsersResponse(name, email, gender, status);
        APIResponse response = requestContext.post(url, RequestOptions.create()
                .setData(user));
        objectMapper = new ObjectMapper();
        userBody = objectMapper.readValue(response.text(), UsersResponse.class);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), CREATED);
        return userBody;
    }

    @Step("Create user by parsed Json file")
    public APIResponse createUser(String url, byte[] data) {
        return requestContext.post(url, RequestOptions.create()
                .setData(data));
    }

    @Step("Delete user")
    public APIResponse deleteUser(String url) {
        APIResponse response = requestContext.delete(url);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), NO_CONTENT);
        return response;
    }

    @Step("Delete user by ID: {1}")
    public APIResponse deleteUserById(String url, String id) {
        APIResponse response = requestContext.delete(url + "/" + id);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), NO_CONTENT);
        return response;
    }

    @Step("Get user by ID: {1}")
    public APIResponse getUserById(String url, String id) {
        APIResponse response = requestContext.get(url + "/" + id);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return requestContext.get(url);
    }

    @Step("Update user Object with random name: {1} and email: {2} via POJO")
    public UsersResponse updateUserObjectWithPojo(String url, String id, Object user) throws IOException {
//    public UsersResponse createUserWithPojo(String url, String name, String email, String gender, String status) throws IOException {
//        user = new UsersResponse(name, email, gender, status);
        APIResponse response = requestContext.put(url + "/" + id, RequestOptions.create()
                .setData(user));
        objectMapper = new ObjectMapper();
        userBody = objectMapper.readValue(response.text(), UsersResponse.class);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
        return userBody;
    }
}
