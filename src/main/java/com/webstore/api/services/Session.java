package com.webstore.api.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;

import static com.webstore.pages.Constants.HttpStatus.CREATED;
import static com.webstore.pages.Constants.HttpStatus.OK;
import static com.webstore.pages.Constants.Jsons.AUTH;

public class Session {

    private APIRequestContext requestContext;
    ObjectMapper objectMapper;
    File file;

    public Session(APIRequestContext requestContext) {
        this.requestContext = requestContext;
    }

    public APIResponse createSession(String url, String personalId) throws IOException {
        file = new File(AUTH);
        String jsonPayload = JsonPath.parse(file)
                .set("$.personalId", personalId)
                .set("$.phoneNumber", null)
                .jsonString();
        APIResponse response = requestContext.post(url, RequestOptions.create()
                .setData(file));
        this.getLogsOutput(response);
//        System.out.println(response.text());
//        System.out.println(response.url());
//        Assert.assertEquals(response.status(), OK);
        return response;
    }

    public APIResponse getAuthResponse(String url) {
        APIResponse response = requestContext.get(url);
        this.getLogsOutput(response);
//        System.out.println(response.text());
//        System.out.println(response.url());
//        Assert.assertEquals(response.status(), OK);
        return response;
    }

    public APIResponse getCookies(String url) {
        APIResponse response = requestContext.get(url);
        this.getLogsOutput(response);
//        System.out.println(response.text());
//        System.out.println(response.url());
//        Assert.assertEquals(response.status(), OK);
        return response;
    }

    public void getLogsOutput(APIResponse response) {
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), OK);
    }
}
