package com.webstore.api.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import com.webstore.api.dao.response.UsersResponse;
import com.webstore.api.dao.response.purchase.PurchaseResponse;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;

import static com.webstore.pages.Constants.Brands.TEST_BRAND_ID;
import static com.webstore.pages.Constants.HttpStatus.CREATED;
import static com.webstore.pages.Constants.Jsons.PURCHASE_REQUEST;
import static com.webstore.pages.Constants.Jsons.USERS_JSON;
import static com.webstore.pages.Constants.Users.TEST_AUTOMATION_EMAIL;

public class Purchases {

    private APIRequestContext apiRequestContext;
    PurchaseResponse purchaseResponse;
    ObjectMapper objectMapper;

    public Purchases(APIRequestContext requestContext) {
        this.apiRequestContext = requestContext;
    }

    /**
     * Create purchase request by upload Json payload updated body
     * @param url
     * @param product
     * @param price
     * @param email
     * @param recurring
     * @return
     * @throws IOException
     */
    public APIResponse create(String url, String product, Integer price, String email, boolean recurring) throws IOException {

        // Get json file for payload
        File file = new File(PURCHASE_REQUEST);
        // Update json parsed file
        String jsonPayload = JsonPath.parse(file)
                .set("$.purchase.products[0].name", product)
                .set("$.purchase.products[0].price", price)
                .set("$.client.email", email)
                .set("$.brand_id", TEST_BRAND_ID)
                .set("$.skip_capture", recurring)
                .jsonString();

        APIResponse response = apiRequestContext.post(url, RequestOptions.create()
                .setData(jsonPayload));
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), CREATED);
        return response;
    }

    /**
     * Creates purchase POST request by upload new Object
     * @param url
     * @param purchase
     * @return
     * @throws IOException
     */
    public PurchaseResponse create(String url, Object purchase) throws IOException {
        APIResponse response = apiRequestContext.post(url, RequestOptions.create()
                .setData(purchase));
        objectMapper = new ObjectMapper();
        purchaseResponse = objectMapper.readValue(response.text(), PurchaseResponse.class);
        System.out.println(response.text());
        System.out.println(response.url());
        Assert.assertEquals(response.status(), CREATED);
        return purchaseResponse;
    }
}
