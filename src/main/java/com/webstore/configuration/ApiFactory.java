package com.webstore.configuration;

import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.RequestOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApiFactory {

    FileInputStream fileInputStream;
    Properties prop;
    private APIRequestContext requestContext;

    public ApiFactory(APIRequestContext requestContext) {
        this.requestContext = requestContext;
    }

    /**
     * Method is used to initialize the properties from config file
     */
    public Properties init_prop() throws IOException {

        try {
            prop = new Properties();
            fileInputStream = new FileInputStream("src/test/java/resources/config/config.properties");
            if (fileInputStream != null) {
                prop.load(fileInputStream);
            } else {
                throw new FileNotFoundException("property file is not found in the classpath");
            }

            prop.load(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileInputStream.close();
        }
        return prop;
    }
}
