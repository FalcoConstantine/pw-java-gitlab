package com.webstore.configuration;

import com.microsoft.playwright.*;
import com.microsoft.playwright.BrowserType.LaunchOptions;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

import io.qameta.allure.Allure;

public class BrowserFactory {

    private static Boolean isTraceEnabled = true;

    Playwright playwright;
    Browser browser;
    BrowserContext browserContext;

    Page page;
    Properties prop;
    FileInputStream fileInputStream;

    private static ThreadLocal<Browser> tlBrowser = new ThreadLocal<>();
    public static ThreadLocal<BrowserContext> tlBrowserContext = new ThreadLocal<>();
    private static ThreadLocal<Page> tlPage = new ThreadLocal<>();
    private static ThreadLocal<Playwright> tlPlaywright = new ThreadLocal<>();

    public static Playwright getPlaywright() {
        return tlPlaywright.get();
    }

    public static Browser getBrowser() {
        return tlBrowser.get();
    }

    public static BrowserContext getBrowserContext() {
        return tlBrowserContext.get();
    }

    public static Page getPage() {
        return tlPage.get();
    }

    public Page initBrowser(Properties prop) {
//        Map<String, String> env = new HashMap<>();
//        env.put("PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD", "1");
//        Playwright.CreateOptions createOptions = new Playwright.CreateOptions();
//        createOptions.setEnv(env);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();
        System.out.println("Dimensions: " + width + " " + height);

//        List<String> list=new ArrayList<String>();
//        list.add("--start-maximized");
//        Map<String, String> env = new HashMap<>();
//        env.put("SELENIUM_REMOTE_URL", "http://testing.net.int:4444/");
//        try(Playwright playwright = Playwright.create(new Playwright.CreateOptions().setEnv(env))){
//            Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setSlowMo(100).setArgs(list));
//            BrowserContext browserContext = browser.newContext();
//            Page page = browserContext.newPage();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
        try {
        // Options
        List<String> list=new ArrayList<String>();
        list.add("--start-maximized");

        tlPlaywright.set(Playwright.create());

           switch (prop.getProperty("browser")) {
               case "chromium":
                   // 1 solution
                   tlBrowser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext(new Browser.NewContextOptions().setViewportSize(width, height)));
                   // 2 solution
//                   tlBrowser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless"))).setSlowMo(50).setChromiumSandbox(true).setArgs(list)));
//                   tlBrowserContext.set(getBrowser().newContext());
                   break;
               case "firefox":
                   tlBrowser.set(getPlaywright().firefox().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext());
                   break;
               case "safari":
                   tlBrowser.set(getPlaywright().webkit().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext());
                   break;
               case "chrome":
                   tlBrowser.set(
                           getPlaywright().chromium().launch(new LaunchOptions().setChannel("chrome").setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext());
                   break;
               case "edge":
                   tlBrowser.set(getPlaywright().chromium().launch(new LaunchOptions().setChannel("msedge").setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext());
                   break;
               case "Galaxy S8":
                   tlBrowser.set(getPlaywright().chromium().launch(new LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext(new Browser.NewContextOptions()
                           .setDeviceScaleFactor(3)
                           .setHasTouch(true)
                           .setIsMobile(true)
                           .setUserAgent("Mozilla/5.0 (Linux; Android 7.0; SM-G950U Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.6045.9 Mobile Safari/537.36")
                           .setViewportSize(360, 740)));
                   break;
               case "Galaxy S8 landscape":
                   tlBrowser.set(getPlaywright().chromium().launch(new LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext(new Browser.NewContextOptions()
                           .setDeviceScaleFactor(3)
                           .setHasTouch(true)
                           .setIsMobile(true)
                           .setUserAgent("Mozilla/5.0 (Linux; Android 7.0; SM-G950U Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.6045.9 Mobile Safari/537.36")
                           .setViewportSize(740, 360)));
                   break;
               case "iPhone 13":
                   tlBrowser.set(getPlaywright().webkit().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext(new Browser.NewContextOptions()
                           .setDeviceScaleFactor(3)
                           .setHasTouch(true)
                           .setIsMobile(true)
                           .setUserAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 15_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4 Mobile/15E148 Safari/604.1")
                           .setViewportSize(390, 664)));
                   break;
               case "iPhone14 Pro landscape":
                   tlBrowser.set(getPlaywright().webkit().launch(new BrowserType.LaunchOptions().setHeadless(Boolean.parseBoolean(prop.getProperty("headless")))));
                   tlBrowserContext.set(getBrowser().newContext(new Browser.NewContextOptions()
                           .setDeviceScaleFactor(2.65)
                           .setHasTouch(true)
                           .setIsMobile(true)
                           .setUserAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 16_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4 Mobile/15E148 Safari/604.1")
                           .setViewportSize(734, 343)));
                   break;
               default:
                   System.out.println("please pass the right browser name......");
                   break;
           }

//           tlBrowserContext.set(getBrowser().newContext());

           tlPage.set(getBrowserContext().newPage());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return getPage();
    }

    /**
     * Method is used to initialize the properties from config file
     */
    public Properties init_prop() throws IOException {

        try {
            prop = new Properties();
            fileInputStream = new FileInputStream("src/test/java/resources/config/config.properties");
            if (fileInputStream != null) {
                prop.load(fileInputStream);
            } else {
                throw new FileNotFoundException("property file is not found in the classpath");
            }

            prop.load(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileInputStream.close();
        }
        return prop;
    }

    /**
     * take screenshot
     *
     */
    public static String takeScreenshot() {
        String path = System.getProperty("user.dir") + "/screenshot/" + System.currentTimeMillis() + ".png";

        byte[] buffer = getPage().screenshot(new Page.ScreenshotOptions().setPath(Paths.get(path)).setFullPage(true));
        String base64Path = Base64.getEncoder().encodeToString(buffer);

        String uuid = UUID.randomUUID().toString();
        byte[] screenshot = getPage().screenshot(new Page.ScreenshotOptions()
                .setPath(Paths.get("build/allure-results/screenshot_" + uuid + "screenshot.png"))
                .setFullPage(true));
        Allure.addAttachment(uuid, new ByteArrayInputStream(screenshot));
        Allure.addAttachment("source.html", "text/html", getPage().content());
//        Allure.addAttachment("source.html", "text/html", getPage().content());
        return base64Path;
    }
}
