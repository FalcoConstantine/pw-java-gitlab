package com.webstore.pages;

import com.microsoft.playwright.Page;
import com.webstore.pages.dtc.DtcMainPage;
import com.webstore.pages.shop.main.ShopPage;

import static com.webstore.pages.Constants.Urls.DTC;
import static com.webstore.pages.Constants.Urls.SHOP;

public class StartTest {

    private Page page;
    Base base;

    public StartTest(Page page) {
        this.page = page;
        this.base = new Base(page);
    }

    public ShopPage onUatShopPage() {
        base.openPage(SHOP);
        return new ShopPage(page);
    }

    public DtcMainPage onUatDtcPage() {
        base.openPage(DTC);
        return new DtcMainPage(page);
    }
}