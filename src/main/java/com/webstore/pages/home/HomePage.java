package com.webstore.pages.home;

import com.microsoft.playwright.Page;
import com.webstore.pages.login.LoginPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Locators.MainPageLocators.search;
import static com.webstore.pages.Locators.MainPageLocators.searchIcon;
import static com.webstore.pages.Locators.MainPageLocators.searchPageHeader;
import static com.webstore.pages.Locators.MainPageLocators.myAccountLink;
import static com.webstore.pages.Locators.MainPageLocators.loginLink;

public class HomePage {
    private Page page;

    public HomePage(Page page) {
        this.page = page;
    }

    @Step("Search with product name: '$product'")
    public HomePage doSearch(String product) {
        page.fill(search, product);
        page.click(searchIcon);
        return new HomePage(page);
    }

    @Step("Get text of the header correct")
    public String getTextOfHeader() {
        return page.textContent(searchPageHeader);
    }

    @Step("Navigate to the Login Page")
    public LoginPage navigateToLoginPage() {
        page.click(myAccountLink);
        page.click(loginLink);
        return new LoginPage(page);
    }
}
