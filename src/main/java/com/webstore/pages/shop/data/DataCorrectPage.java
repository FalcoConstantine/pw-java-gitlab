package com.webstore.pages.shop.data;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.offers.OffersPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Constants.Languages.*;
import static com.webstore.pages.Locators.DataCorrect.*;

public class DataCorrectPage {

    private Page page;
    Base base;

    public DataCorrectPage(Page page) {
        this.page = page;
        this.base = new Base(page);
    }

    @Step("Press \'Yes, all good\' button")
    public OffersPage pressYesAllGoodButton(String language) {
        switch(language) {
            case ENG:
                base.clickElement(YES_ALL_GOOD_BUTTON_EN);
                break;
            case LT:
                base.clickElement(YES_ALL_GOOD_BUTTON_LT);
                break;
            case EE:
                base.clickElement(YES_ALL_GOOD_BUTTON_EE);
                break;
//            default:
//                System.out.println(String.format("Language is wrong ...");
        }
        return new OffersPage(page);
    }

    @Step("Is logo visible")
    public boolean isLogoVisible() {
        return base.getFirstElement(LOGO).isVisible();
    }

    @Step("Is page title visible")
    public boolean isTitleVisible() {
        return base.getFirstElement(TITLE).isVisible();
    }

    @Step("Is phone: {1} visible on the page")
    public boolean isPhoneVisible() {
        return base.getFirstElement(CONTACT_DATA_CONTENT).isVisible();
    }

    @Step("Get phone number")
    public String getPhoneNumber() {
        return base.getFirstElement(CONTACT_DATA_CONTENT).textContent();
    }

    @Step("Is email: {2} visible on the page")
    public boolean isEmailVisible() {
        return base.getLastElement(CONTACT_DATA_TITLE).isVisible();
    }

    @Step("Get email address")
    public String getEmailAddress() {
        return base.getLastElement(CONTACT_DATA_CONTENT).textContent();
    }
}
