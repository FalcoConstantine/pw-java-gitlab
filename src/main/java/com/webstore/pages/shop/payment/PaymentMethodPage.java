package com.webstore.pages.shop.payment;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import com.webstore.pages.Base;
import com.webstore.pages.shop.auth.AuthenticationPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Constants.Payments.PAY_LATER_LT;
import static com.webstore.pages.Constants.Payments.PAY_LATER_LV;

public class PaymentMethodPage {
    private Page page;
    Base base;

    public PaymentMethodPage(Page page) {
        this.page = page;
        this.base = new Base(page);
    }

    @Step("Press LV Pay Later payment method button")
    public AuthenticationPage pressLvPayLaterPaymentMethodButton() {
        base.clickElement(PAY_LATER_LV);
        return new AuthenticationPage(page);
    }

    @Step("Press LV Pay Later payment method button")
    public AuthenticationPage pressLtPayLaterPaymentMethodButton() {
            base.clickElement(PAY_LATER_LT);
        return new AuthenticationPage(page);
    }
}