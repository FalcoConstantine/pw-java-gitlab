package com.webstore.pages.shop.agreement;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.CompletePaymentPage.CompletePaymentPage;
import com.webstore.pages.shop.processing.ProcessingPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Constants.Languages.ENG;
import static com.webstore.pages.Constants.Languages.LT;
import static com.webstore.pages.Localisation.AgreementPage.SIGN_THE_AGREEMENT_BUTTON_TEXT;
import static com.webstore.pages.Localisation.AgreementPage.SIGN_THE_AGREEMENT_BUTTON_TEXT_LT;
import static com.webstore.pages.Localisation.OffersPage.GET_AGREEMENT_BUTTON_EN;
import static com.webstore.pages.Localisation.OffersPage.GET_AGREEMENT_BUTTON_LT;
import static com.webstore.pages.Locators.AgreementPage.*;
import static com.webstore.pages.Locators.OffersPage.AGREEMENT_BUTTON;
import static com.webstore.pages.Locators.OffersPage.FLEX_CONTAINER_HORIZONTAL;

public class AgreementPage {

    private Page page;
    Base base;

    public AgreementPage(Page page) {
        this.page = page;
        this.base = new Base(page);
        base.waitLocatorToHaveCount(CONSENT_CHECKBOX, 1, 150000); // 150 sec.
        assert base.getText(PROGRESS_BAR_STEPS).equals("4/4");
    }

    @Step("Wait for an agreement to prepare")
    public AgreementPage pressAgreementConsent() {
        base.waitToBeVisible(CONSENT_CHECKBOX, 60000); // 60 sec.
        base.clickElement(CONSENT_CHECKBOX);
        return new AgreementPage(page);
    }

    public boolean isDownPaymentBannerVisible() {
        return base.isLocatorVisibleOnPage(FLEX_CONTAINER_HORIZONTAL);
    }

    @Step("Sign the agreement")
    public CompletePaymentPage signTheAgreement(String language) {
        switch(language) {
            case ENG:
                base.getElementByTextAndId(AGREEMENT_SIGNING_BUTTON, SIGN_THE_AGREEMENT_BUTTON_TEXT, 1).click();
                break;
            case LT:
                base.getElementByTextAndId(AGREEMENT_SIGNING_BUTTON, SIGN_THE_AGREEMENT_BUTTON_TEXT_LT, 1).click();
        }
        return new CompletePaymentPage(page);
    }
}
