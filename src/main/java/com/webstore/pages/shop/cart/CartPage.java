package com.webstore.pages.shop.cart;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.main.ShopPage;
import com.webstore.pages.shop.payment.PaymentMethodPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Constants.CartPage.CART_HEADER_TXT;
import static com.webstore.pages.Locators.CartPageLocators.*;

import static com.webstore.pages.Locators.ShopPageLocators.CLOSE_DEMO_PANEL;

public class CartPage {

    private Page page;
    Base base;

    public CartPage (Page page) {
        this.page = page;
        this.base = new Base(page);
//        assert base.getElement(CART_HEADER).textContent().contains(CART_HEADER_TXT);
    }

    @Step("Close info banner")
    public CartPage closeBanner() {
        base.clickElement(CLOSE_DEMO_PANEL);
        return new CartPage(page);
    }

    @Step("Press checkout button")
    public CartPage pressCheckoutButton() {
        base.clickElement(CART_CHECKOUT_BUTTON);
        return new CartPage(page);
    }

    @Step("Select guest checkout method")
    public CartPage selectGuestCheckoutMethod() {
        base.clickLastElement(SELECT_USER);
        return new CartPage(page);
    }

    @Step("Press on choose payment method button")
    public PaymentMethodPage pressOnChoosePaymentMethodButton() {
        base.clickElement(CHOOSE_PAYMENT_METHOD);
        return new PaymentMethodPage(page);
    }
}