package com.webstore.pages.shop.offers;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.agreement.AgreementPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Constants.Languages.ENG;
import static com.webstore.pages.Constants.Languages.LT;
import static com.webstore.pages.Localisation.OffersPage.GET_AGREEMENT_BUTTON_EN;
import static com.webstore.pages.Localisation.OffersPage.GET_AGREEMENT_BUTTON_LT;
import static com.webstore.pages.Locators.OffersPage.*;

public class OffersPage {

    private Page page;
    Base base;

    public OffersPage(Page page) {
        this.page = page;
        this.base = new Base(page);
        base.waitToHaveText(PROGRESS_BAR_STEPS, "3/4", 20000);
        base.waitLocatorToHaveCount(GREY_TEXT, 0, 310000);
    }

    @Step("Press \'Get agreement\' button")
    public AgreementPage pressAgreementButton(String language) {
        switch(language) {
            case ENG:
                base.getElementByTextAndId(AGREEMENT_BUTTON, GET_AGREEMENT_BUTTON_EN, 1).click();
                break;
            case LT:
                base.getElementByTextAndId(AGREEMENT_BUTTON, GET_AGREEMENT_BUTTON_LT, 1).click();
        }
        return new AgreementPage(page);
    }

    // booleans for assertions has no Steps !!!!
    public boolean isDownPaymentBannerVisible() {
        return base.isLocatorVisibleOnPage(FLEX_CONTAINER_HORIZONTAL);
    }


}
