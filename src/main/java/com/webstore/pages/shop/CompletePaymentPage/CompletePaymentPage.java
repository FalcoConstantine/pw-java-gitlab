package com.webstore.pages.shop.CompletePaymentPage;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;

import static com.webstore.pages.Localisation.CompletePaymentPage.MAKE_DEPOSIT_BUTTON_TEXT;
import static com.webstore.pages.Locators.CompletePaymentPage.MAKE_DEPOSIT_BUTTON;
import static com.webstore.pages.Locators.CompletePaymentPage.PURCHASE_PAYMENT_COMPLETED;

public class CompletePaymentPage {

    private Page page;
    Base base;

    public CompletePaymentPage(Page page) {
        this.page = page;
        this.base = new Base(page);
        base.waitLocatorToHaveCount(PURCHASE_PAYMENT_COMPLETED, 1, 60000);
    }

    public boolean isMakeFirstDepositButtonVisible() {
        return base.getElementByTextAndId(MAKE_DEPOSIT_BUTTON, MAKE_DEPOSIT_BUTTON_TEXT, 1).isVisible();
    }
}
