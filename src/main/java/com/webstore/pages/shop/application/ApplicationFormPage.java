package com.webstore.pages.shop.application;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.data.DataCorrectPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Localisation.ApplicationFormPage.*;
import static com.webstore.pages.Locators.ApplicationFormPAgeLocators.*;

public class ApplicationFormPage {

    private Page page;
    Base base;

    public ApplicationFormPage(Page page) {
        this.page = page;
        this.base = new Base(page);
//        assert base.getElement(HEADER).textContent().contains(APPLICATION_HEADER_TEXT_EN);
    }

    @Step("Select address: {1}")
    public ApplicationFormPage selectAddress(String address) {
        base.fillText(ADDRESS, address);
        base.clickFirstElement(ADDRESS_ITEM);
        return  new ApplicationFormPage(page);
    }

    @Step("Fill monthly income: {1}")
    public ApplicationFormPage fillMonthlyIncome(String amount) {
        base.fillText(MONTHLY_INCOME, amount);
        return new ApplicationFormPage(page);
    }

    @Step("Fill credit liabilities: {1}")
    public ApplicationFormPage fillCreditLiabilities(String credit) {
        base.fillText(LIABILITIES, credit);
        return new ApplicationFormPage(page);
    }

    // NO STEPS for booleans and assertions !!!!
    public boolean getDownPaymentField() {
//        base.debug();
        return base.isLocatorVisibleOnPage(DOWN_PAYMENT);
    }

    @Step("Fill down payment amount: {1}")
    public ApplicationFormPage fillDownPaymentAmount(String amount) {
        base.fillText(DOWN_PAYMENT, amount);
        return new ApplicationFormPage(page);
    }

    @Step("Select payment day: {1}")
    public ApplicationFormPage selectPaymentDay(String day) {
        base.clickElementByText(RADIO_DROPDOWN, day);
        return new ApplicationFormPage(page);
    }

    @Step("Provide agreement to store data")
    public ApplicationFormPage provideAgreementFullData() {
        base.clickElement(AGREEMENT_FULL_DATA);
        return new ApplicationFormPage(page);
    }

    @Step("Provide agreement to share data")
    public ApplicationFormPage provideAgreementShareData() {
        base.clickElement(AGREEMENT_SHARE_DATA);
        return new ApplicationFormPage(page);
    }

    @Step("Provide consent in LT")
    public ApplicationFormPage provideConsentInLT() {
        page.getByLabel(CONSENT_TEXT_LT).check();
        return new ApplicationFormPage(page);
    }

    @Step("Select marital status: {1}")
    public ApplicationFormPage selectMaritalStatus(String maritalStatus) {
        base.clickElementByText(RADIO_DROPDOWN, maritalStatus);
        return new ApplicationFormPage(page);
    }

    @Step("Select number of dependents: {1}")
    public ApplicationFormPage selectNumberOfDependents(String dependents) {
        base.clickElementByText(RADIO_DROPDOWN, dependents);
        return new ApplicationFormPage(page);
    }

    @Step("Select income type: {1}")
    public ApplicationFormPage selectIncomeType(String source) {
        base.clickElementByText(RADIO_DROPDOWN, source);
        return new ApplicationFormPage(page);
    }

    @Step("Fill phone number: {phone}")
    public ApplicationFormPage fillPhoneNumber(String phone) {
        base.removeText(PHONE);
        base.fillText(PHONE, phone);
        return new ApplicationFormPage(page);
    }

    @Step("Fill email: {email}")
    public ApplicationFormPage fillEmailAddress(String email) {
        base.removeText(EMAIL);
        base.fillText(EMAIL, email);
        return new ApplicationFormPage(page);
    }

    @Step("Enter employer name: {1}")
    public ApplicationFormPage selectEmployerName(String name) {
        base.removeText(EMPLOYER);
        base.fillText(EMPLOYER, name);
        return new ApplicationFormPage(page);
    }

    public DataCorrectPage pressContinueButton() {
        base.clickElement(APPLICATION_CONTINUE_BUTTON);
        return new DataCorrectPage(page);
    }
}