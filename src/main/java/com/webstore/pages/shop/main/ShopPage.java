package com.webstore.pages.shop.main;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import com.webstore.pages.Base;
import com.webstore.pages.Pages;
import com.webstore.pages.shop.cart.CartPage;
import io.qameta.allure.Step;

import java.util.List;

import static com.webstore.pages.Constants.Items.AMOUNT_1100;
import static com.webstore.pages.Constants.Shop.CART;
import static com.webstore.pages.Constants.ShopPage.DEMO_STORE_TXT;
import static com.webstore.pages.Locators.ShopPageLocators.*;

public class ShopPage {
    private Page page;
    Base base;

    public ShopPage (Page page) {
        this.page = page;
        this.base = new Base(page);
        assert base.getFirstElement(SHOP_NAME).textContent().contains(DEMO_STORE_TXT);
    }

    @Step("Close info banner")
    public ShopPage closeBanner() {
        base.clickElement(CLOSE_DEMO_PANEL);
        return new ShopPage(page);
    }

    @Step("Add item: {1} to cart")
    public ShopPage addItemToCartByPrice(String amount) {
        base.getElement(ADD_TO_CARD).nth(base.getLocatorIndexByText(PRODUCT_INNER_CONTAINER, amount)).click();
        return new ShopPage(page);
    }

    @Step("Open cart")
    public CartPage openCart() {
        base.clickElement(CART_LOGO);
        return new CartPage(page);
    }
}