package com.webstore.pages.shop.auth;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;
import com.webstore.pages.shop.application.ApplicationFormPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Localisation.AuthenticationPage.AUTHENTICATION_HEADER_TEXT_EN;
import static com.webstore.pages.Locators.ApplicationFormPAgeLocators.HEADER;
import static com.webstore.pages.Locators.AuthenticationPageLocators.*;

public class AuthenticationPage {

    private Page page;
    Base base;

    public AuthenticationPage(Page page) {
        this.page = page;
        this.base = new Base(page);
        assert base.getElement(HEADER).textContent().contains(AUTHENTICATION_HEADER_TEXT_EN);
    }

    @Step("Select Demo Smart-ID authentication")
    public AuthenticationPage selectDemoSmartIdAuth() {
        base.clickElement(MOCK_SMART_ID_LOCATOR);
        return new AuthenticationPage(page);
    }

    @Step("Fill customer personal code {1}")
    public AuthenticationPage fillCustomerPersonalCode(String personalId) {
        base.fillText(MOCK_SMART_ID, personalId);
        return new AuthenticationPage(page);
    }

    @Step("Press Continue button")
    public ApplicationFormPage pressContinueButton() {
        base.clickElement(AUTH_CONTINUE_BUTTON);
        return new ApplicationFormPage(page);
    }
}