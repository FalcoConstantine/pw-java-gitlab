package com.webstore.pages.shop.processing;

import com.microsoft.playwright.Page;
import com.webstore.pages.Base;

public class ProcessingPage {

    private Page page;
    Base base;

    public ProcessingPage(Page page) {
        this.page = page;
        this.base = new Base(page);
    }
}
