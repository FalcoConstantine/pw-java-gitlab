package com.webstore.pages;

public class Locators {

    public static class MainPageLocators {
        public static String search = "input[name='search']";
        public static String searchIcon = "div#search button";
        public static String searchPageHeader = "div#content h1";
        public static String loginLink = "a:text('Login')";
        public static String myAccountLink = "//span[normalize-space()='My Account']";
    }

    public static class LoginPageLocators {
        public static String emailId = "//input[@id='input-email']";
        public static String password = "//input[@id='input-password']";
        public static String loginBtn = "//input[@value='Login']";
        public static String forgotPwdLink = "(//a[normalize-space()='Forgotten Password'])[1]";
        public static String logoutLink = "//a[@class='list-group-item'][normalize-space()='Logout']";
    }

    public static class ShopPageLocators {

        public static final String SHOP_NAME = ".shop-name"; // class

        public static final String CLOSE_DEMO_PANEL = ".close-button"; // class

        public static final String ADD_TO_CARD = "xpath=//div[@class = 'add-to-cart clickable']"; // custom xpath

//        public static final String CART_HEADER = ".section-header";


        public static final String PRODUCT_INNER_CONTAINER = "xpath=//div[@class = 'inner-container']"; // custom xpath

        public static final String CART_LOGO = ".full_cart"; // class


//        public static final String ITEM = "//div[@class = 'add-to-cart clickable']";

        public static final String ITEM_LOCATOR = "(//div[@class='add-to-cart clickable'][normalize-space()='Add to cart'])[%s]";

//        public static final String PRODUCT_INNER_CONTAINER = "//div[@class = 'inner-container']";
        public static final String ITEM_1100 = "div:nth-child(2) > .add-to-cart-highlight > .add-to-cart";
        public static final String ITEM_200 = "div:nth-child(5) > .add-to-cart-highlight > .add-to-cart";
    }

    public static class CartPageLocators {

        public static final String CART_HEADER = "xpath=.//div[@class='section-header']"; // class

        public static final String CART_CHECKOUT_BUTTON = ".cart-checkout-button"; // class
        public static final String SELECT_USER ="#secondary-block-button"; // id
        public static final String CHOOSE_PAYMENT_METHOD = "text=Choose payment method"; // text
        public static final String PAYMENT_METHOD_HEADER = ".payment-method-title";
    }

    public static class AuthenticationPageLocators {
        public static final String MOCK_SMART_ID_LOCATOR = "xpath=.//input[@value = 'Mock_Smart-ID']"; //xpath
        public static final String MOCK_SMART_ID= "#person-code-input-Mock_Smart-ID"; //id
        public static final String AUTH_CONTINUE_BUTTON = "xpath=//button[@class = 'select-authentication-method form-button']"; //xpath
    }

    public static class ApplicationFormPAgeLocators {

//        public static final String RADIO_DROPDOWN = "xpath=.//div[@class='klix-radio-dropdown']";
        public static final String ADDRESS = "#address-service-input";
        public static final String ADDRESS_ITEM = "xpath=(.//div[@class = 'address-dropdown-list']/div)";

        public static final String RADIO_DROPDOWN = "xpath=.//div[normalize-space()='%s']";
        public static final String MONTHLY_INCOME = "#monthlyIncome";
        public static final String LIABILITIES = "#monthlyCreditLiabilities";

        public static final String DOWN_PAYMENT = "#preferredDownPaymentAmount";
        public static  final String EMPLOYER = "#employerName";
        public static final String EMAIL = "#email";
        public static final String PHONE = "#phoneNumber";
        public static final String AGREEMENT_FULL_DATA = "#agreeToTruthfulData";

        public static final String AGREEMENT_SHARE_DATA = "#agreeToDataSharingWithFinancingInstitutions";

        public static final String APPLICATION_CONTINUE_BUTTON = "xpath=((.//div[@class = 'action-button'])/button)[3]";

        public static final String HEADER = ".progress-bar-step-title";
    }

    public static class DataCorrect {
        public static final String YES_ALL_GOOD_BUTTON_EN = "text=Yes, all good";
        public static final String YES_ALL_GOOD_BUTTON_LT = "text=Duomenys teisingi";
        public static final String YES_ALL_GOOD_BUTTON_EE = "text=Jah, kõik on hästi";

        public static final String LOGO = ".klix-popup--klix-logo";
        public static final String TITLE = ".klix-popup--title";
        public static final String CONTACT_DATA_TITLE = ".contact-data-title";
        public static final String CONTACT_DATA_CONTENT = ".contact-data-content";

    }

    public static class OffersPage {
        public static final String PROGRESS_BAR_STEPS = ".progress-bar-steps";
        public static final String GREY_TEXT = ".grey-text";

        public static final String AGREEMENT_BUTTON = "(//button[normalize-space()='%s'])[%d]";

        public static final String FLEX_CONTAINER_HORIZONTAL = ".flex-container-horizontal";
    }

    public static class AgreementPage {
        public static final String CONSENT_CHECKBOX = "id=consent";
        public static final String PROGRESS_BAR_STEPS = ".progress-bar-steps";

        public static final String AGREEMENT_SIGNING_BUTTON = "(//button[normalize-space()='%s'])[%d]";
    }

    public static class CompletePaymentPage {

        public static final String PURCHASE_PAYMENT_COMPLETED = "xpath=(.//div[@class = 'text']/div)[1]";
        public static final String MAKE_DEPOSIT_BUTTON = "(//button[normalize-space()='%s'])[%d]";
    }



    public static String getElementByClassname = "//*[@class='%s']";
    public static String getElementsByText = "//*[text() = '%s']";
    public static String getElementByTagName = "//*[@name='%s']";
    public static String getElementByElementId = "//*[@id='%s']";
    public static String getElementByDataId = "//*[data-id='%s']";
    public static String getElementByContainedText = "//*[contains(text(),'%s')]";

    public static String getElementsByTextAndArrId = "(//*[text() = '%s'])[%d]";
    public static String getElementByClassNameAndArrId = "(//*[@class='%s'])[%d]";
    public static String getElementByClassNameAndText = "//*[@class='%s'][text()='%s']";
    public static String getElementByContainedTextAndArrId = "(//*[contains(text(),'%s')])[%d]";
    public static String getElementsByClassNameAndArrId = "(//*[@class = '%s'])[%d]";

    public static String getElementByClassNmeAndDataId = "//*[@class='%s'][@data-id='%s']";
    public static String getGetElementByClassNameAndImageAltAttribute = "//*[@class = '%s']//img[@alt = '%s']";
    public static String getElementByClassNameAndTextAndArrId = "(//*[@class = '%s']//*[text() = '%s'])[%d]";
    public static String getElementByClassNameContainedText = "//*[@class = '%s'][contains(text(),'%s')]";
    public static String getElementByElementIdAndName = "//*[@id='%s'][@name='%s']";
    public static String getElementByRadioButtonBlockAndId = "(//*[@class = '%s'])[%d]//*[@class = '%s']//input[@id = '%s']";
}
