package com.webstore.pages;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.javafaker.Faker;
import com.google.gson.JsonArray;
import com.webstore.utils.Randomiser;

import java.util.Random;

/**
 * Class contains global constants
 */
public class Constants {

    static Random random = new Random();
    static Faker faker = new Faker();
    static Randomiser randomiser = new Randomiser(faker, random);
    public static class PageUrls {
        public static final String INSPECTOR_URL = "java/docs/debug#playwright-inspector";
        public static final String LOGIN = "index.php?route=account/login";
    }

    public static class ApiPaths {
        public static final String BASE = "https://gorest.co.in";
        public static final String GET_USERS = "/public/v2/users";

        public static final String PURCHASES = "purchases/";
        public static final String AUTHENTICATION = "https://api.uat.klix.app/financing/orders/0a673858-01fa-45df-b12e-3a1e8de11aff";
        public static final String AUTH = "/authenticators/Mock_Smart-ID/authentications";
        public static final String SESSION = "/session";
        public static final String CONSUMER_CREDIT = "/consumer-credit";
    }

    public static class MainPage {
        public static final String TITLE_MAIN_PAGE = "Your Store";
    }

    public static class LoginPage {
        public static final String TITLE_LOGIN_PAGE = "Account Login";
    }

    public static class ShopPage {
        public static final String DEMO_STORE_TXT = "demostore";
    }
    public static class CartPage {
        public static final String CART_HEADER_TXT = "Cart";
    }


    public static class HttpStatus {
//        REST API Http Response Codes
//                200: OK. Everything worked as expected.
//                201: A resource was successfully created in response to a POST request. The Location header contains the URL pointing to the newly created resource.
//                204: The request was handled successfully and the response contains no body content (like a DELETE request).
//                304: The resource was not modified. You can use the cached version.
//                400: Bad request. This could be caused by various actions by the user, such as providing invalid JSON data in the request body etc.
//                401: Authentication failed.
//                403: The authenticated user is not allowed to access the specified API endpoint.
//                404: The requested resource does not exist.
//                405: Method not allowed. Please check the Allow header for the allowed HTTP methods.
//                415: Unsupported media type. The requested content type or version number is invalid.
//                422: Data validation failed (in response to a POST request, for example). Please check the response body for detailed error messages.
//                429: Too many requests. The request was rejected due to rate limiting.
//                500: Internal server error. This could be caused by internal program errors.

        public static final Integer OK = 200;
        public static final Integer CREATED = 201;
        public static final Integer NO_CONTENT = 204;
        public static final Integer UNPROCESSABLE_ENTITY = 422;
        public static final Integer UNAUTHORIZED = 401;
        public static final Integer BAD_REQUEST = 400;
        public static final Integer CONFLICT = 409;
        public static final Integer FORBIDDEN = 403;
    }

    public static class Users {
        public static final String NAME = randomiser.getRandomName();
        public static final String EMAIL = randomiser.getRandomEmail();

        public static final String TEST_AUTOMATION_EMAIL = "test@automation.email";
    }

    public static class Jsons {
        public static final String USERS_JSON = "src/main/java/com/webstore/api/resources/users.json";
        public static final String PURCHASE_REQUEST = "src/main/java/com/webstore/api/jsons/purchases/purchase_request.json";
        public static final String VISA = "src/main/java/com/webstore/api/resources/constants/cards/visa.json";
        public static final String MASTERCARD = "src/main/java/com/webstore/api/resources/constants/cards/mastercard.json";
        public static final String AUTH = "src/main/java/com/webstore/api/jsons/klix/auth.json";
        public static final String STATE = "state.json";
    }

    public static class Shop {
        public static final String CART = "cart";
    }

    public static class Cart {
        public static final String CHECKOUT_TEXT = "Checkout";
        public static final String CHOOSE_PAYMENT_METHOD_TEXT = "Choose payment method";
    }

    public static class Payments {
        public static final String PAY_LATER_LV = "xpath=.//img[@alt='Pay later']";
        public static final String PAY_LATER_LT = "xpath=.//img[@alt='Pay later LT']";

    }

    public static class Languages {
        public static final String LT = "LT";
        public static final String LV = "LV";
        public static final String EE = "EE";
        public static final String ENG = "ENG";
        public static final String RU = "RU";
    }

    public static class PersonalData {
        public static final String LV_LATEKO = "000000-99961";
        public static final String LT_CITADELE = "38509060436";
        public static final String LV_CITADELE = "201090-10113";
        public static final String EE_CITADELE = "00000000000";
        public static final String LV_VIZIA = "071162-14214";
    }

    public static class Address {
        public static final String LV_ADDRESS = "1. Dāliju iela 1, Daugavpils, Latvija";
        public static final String LT_ADDRESS = "Baskų gatvė 14, Vilnius, Naujosios Vilnios sen., Vilniaus miesto sav., Lietuva";
    }

    public static class Phone {
        public static final String LV_PLACEHOLDER = "21234567";
        public static final String LV_PHONE = "25555555";
    }

    public static class Email {
        public static final String LV_EMAIL = "test_lv@test.lv";
        public static final String LT_EMAIL = "test_lt@test.lt";
    }

    public static class Employers {
         public static final String LV_EMPLOYER = "Citadele Banka AS";
    }

    public static class Urls {
        public static final String SHOP = "https://shop.uat.klix.app/";
        public static final String DTC = "https://klix.app/en/dtc-stage/";
    }

    public static class Items {
        public static final String AMOUNT_1100 = "1100";
    }

    public static class PaymentDates {
        public static final String DATE_1 = "1";
        public static final String DATE_5 = "5";
        public static final String DATE_10 = "10";
        public static final String DATE_15 = "15";
        public static final String DATE_20 = "20";
        public static final String[] ALL_PAYMENT_DAYS = { DATE_1, DATE_5, DATE_10, DATE_15, DATE_20 };
    }

    public static class Brands {
        public static final String TEST_BRAND_ID = "702314b8-dd86-41fa-9a22-510fdd71fa92";
    }

}
