package com.webstore.pages;

import com.webstore.configuration.BrowserFactory;
import com.webstore.pages.account.AccountPage;
import com.webstore.pages.dtc.DtcMainPage;
import com.webstore.pages.home.HomePage;
import com.webstore.pages.login.LoginPage;
import com.webstore.pages.shop.CompletePaymentPage.CompletePaymentPage;
import com.webstore.pages.shop.agreement.AgreementPage;
import com.webstore.pages.shop.application.ApplicationFormPage;
import com.webstore.pages.shop.auth.AuthenticationPage;
import com.webstore.pages.shop.cart.CartPage;
import com.webstore.pages.shop.data.DataCorrectPage;
import com.webstore.pages.shop.main.ShopPage;
import com.webstore.pages.shop.offers.OffersPage;
import com.webstore.pages.shop.payment.PaymentMethodPage;
import com.webstore.pages.shop.processing.ProcessingPage;

import java.util.Properties;

public class Pages {

    protected ProcessingPage processingPage;
    protected AgreementPage agreementPage;

    protected CompletePaymentPage completePaymentPage;
    protected OffersPage offersPage;
    protected StartTest startTest;

    protected Properties prop;
    protected HomePage homePage;
    protected LoginPage loginPage;
    protected AccountPage accountPage;

    protected ShopPage shopPage;

    protected CartPage cartPage;

    protected PaymentMethodPage paymentMethodPage;

    protected AuthenticationPage authenticationPage;

    protected ApplicationFormPage applicationFormPage;

    protected DtcMainPage dtcMainPage;

    protected DataCorrectPage dataCorrectPage;
}
