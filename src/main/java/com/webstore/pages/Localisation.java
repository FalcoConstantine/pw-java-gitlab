package com.webstore.pages;

/**
 * Class contains translations
 */
public class Localisation {

    public static class MainPage {

        public static final String[] SEARCH_HEADER_TEXT = {"Search - Macbook", "Search - iMac", "Search - Samsung"};
        public static final String SEARCH_HEADER_TEXT_MACBOK = "Search - Macbook";
        public static final String SEARCH_HEADER_TEXT_IMAC = "Search - iMac";
        public static final String SEARCH_HEADER_TEXT_SAMSUNG = "Search - Samsung";
        public static final String SEARCH_HEADER_TEXT_SEARCH = "Search - ";
    }

    public static class LoginPage {
        public static final String LOGIN_BTN_EN = "Login";
        public static final String LOGIN_BTN_LT = "Prisijungti";
        public static final String FORGOT_PWD_LINK_EN = "Forgotten Password";
        public static final String FORGOT_PWD_LINK_LT = "Pamirštas slaptažodis";
    }

    public static class CartPage {
        public static final String PAYMENT_METHOD_HEADER_TEXT = "Choose your payment method";
        public static final String CART_HEADER_TXT = "Cart";
    }

    public static class AuthenticationPage {
        public static final String MOCK_SMART_ID_EN = "Mock Smart-ID for testing purposes";
        public static final String MOCK_SMART_ID_LT = "Išmėginkite „Smart-ID“ testavimo tikslais";
//        public static final String CONTINUE_BTN_EN = "Continue";
        public static final String CONTINUE_BTN_LT = "Tęsti";
        public static final String AUTHENTICATION_HEADER_TEXT_EN = "Login to continue";
    }

    // ==================================================================================
    public static class ApplicationFormPage {
        public static final String APPLICATION_HEADER_TEXT_EN = "Fill the application";

        public static final String LIABILITIES_TEXT_EN = "^Current monthly credit liabilities amount$";
        public static final String LIABILITIES_TEXT_LT = "^Mėnesio finansinių įsipareigojimų suma$";
        public static final String CONSENT_TEXT_EN = "I will make the first payment at the time of receiving the goods in cash or by card";
        public static final String CONSENT_TEXT_LT = "Patvirtinu, kad visa pateikta informacija yra pilna ir teisinga.";
    }

    public static class MaritalStatus {
        public static final String MARRIED_EN = "Married";
        public static final String DIVORCED_EN = "Divorce";
        public static final String SINGLE_EN = "Single";
        public static final String WIDOWER_WIDOW_EN = "Widower/Widow";
        public static final String COHABITING_EN = "Cohabiting";
    }

    public static class NumberOfDependents {
        public static final String NULL = "0";
        public static final String ONE = "1";
        public static final String TWO = "2";
        public static final String THREE = "3";
        public static final String FOUR_AND_MORE = "4 and more";
    }

    public static class IncomeSource {
        public static final String SALARY_EN = "Salary";
        public static final String PENSION_EN = "Pension";
        public static final String ECONOMIC_ACTIVITIES_EN = "Income from economic activities";
    }

        // ====================================================================================

//    public static class DataCorrect {
//        public static final String YES_ALL_GOOD_BUTTON_EN = "text=Yes, all good";
//        public static final String YES_ALL_GOOD_BUTTON_LT = "text=Duomenys teisingi";
//        public static final String YES_ALL_GOOD_BUTTON_EE = "text=Jah, kõik on hästi";
//    }

        public static class OffersPage {
            public static final String GET_AGREEMENT_BUTTON_EN = "Get agreement";
            public static final String GET_AGREEMENT_BUTTON_LT = "Paruošti sutartį";
    }

    public static class AgreementPage {
        public static final String SIGN_THE_AGREEMENT_BUTTON_TEXT = "Sign the agreement";
        public static final String SIGN_THE_AGREEMENT_BUTTON_TEXT_LT = "Pasirašyti sutartį";
    }

    public static class CompletePaymentPage {
        public static final String MAKE_DEPOSIT_BUTTON_TEXT = "Make the first deposit";
    }
}
