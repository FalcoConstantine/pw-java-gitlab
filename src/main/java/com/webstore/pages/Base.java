package com.webstore.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;

import java.util.List;
import java.util.stream.Collectors;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class Base {
    Page page;

    public Base(Page page) {
        this.page = page;
    }

    public void debug() {
        page.pause();
    }

    public void openPage(String url) {
        page.navigate(url);
    }

    public Locator getElement(String locator) {
        return page.locator(locator);
    }

    @SafeVarargs
    public final <T> Locator getElement(String locator, T... args) {
        return page.locator(String.format(locator, args));
    }

    public Locator getFirstElement(String locator) {
        return page.locator(locator).first();
    }

    public Locator getLastElement(String locator) {
        return page.locator(locator).last();
    }

    public Locator getElementByText(String locator, String text) {
        return page.locator(String.format(locator, text));
    }

    @SafeVarargs
    public final <T> Locator getElementByText(String locator, T... args) {
        return page.locator(String.format(locator, args));
    }

    public Locator getElementByTextAndId(String locator, String text, Integer id) {
        return page.locator(String.format(locator, text, id));
    }

    public void clickElementByText(String locator, String text) {
        getElementByText(locator, text).first().click();
    }

    public List<Locator> getListOfElements(String locator) {
        return getElement(locator).all();
    }

    public Integer getCountOfElements(String locator) {
        return getElement(locator).count();
    }

    public Integer getLocatorIndexByText(String locator, String text) {
//        Integer newElement = null;
//        for(Locator element : getListOfElements(locator)) {
//            if (text == element.textContent()) {
//                newElement = element;
//                break;
//            }
//        }
//        return newElement;
        Integer iteration = null;
        for(int i = 0; i < getCountOfElements(locator); i++) {
            if (getElement(locator).nth(i).textContent().contains(text)) {
//                System.out.println("TEXT: " +  getElement(locator).nth(i).textContent());
//                System.out.println("Iteration: " + i);
                    iteration = i;
                    break;
                }
            }
        return iteration;
    }

    public void clickElement(String locator) {
        getElement(locator).click();
    }

    public void clickFirstElement(String locator) {
        getFirstElement(locator).click();
    }

    public void clickLastElement(String locator) {
        getLastElement(locator).click();
    }

    public String getText(String locator) {
        return getElement(locator).first().textContent();
    }

    public void fillText(String locator, String text) {
        getElement(locator).fill(text);
    }

    public void selectText(String locator) {
        getElement(locator).press("Control+a");
    }

    public void removeText(String locator) {
        selectText(locator);
        getElement(locator).fill("");
    }

    public void witForSelector(String locator) {
        page.waitForSelector(locator, new Page.WaitForSelectorOptions().setTimeout(10000));
    }

    public void waitToHaveText(String locator, String text, double timeout) {
        assertThat(getFirstElement(locator)).hasText(text, new LocatorAssertions.HasTextOptions().setTimeout(timeout));
    }

    public void waitToContainText(String locator, String text) {
        assertThat(getFirstElement(locator)).containsText(text, new LocatorAssertions.ContainsTextOptions().setTimeout(20000));
    }

    public void waitToBeVisible(String locator, double timeout) {
       assertThat(getFirstElement(locator)).isVisible(new LocatorAssertions.IsVisibleOptions().setTimeout(timeout));
    }

    public List<String> selectOption(String locator, String option) {
        return getElement(locator).selectOption(option).stream().collect(Collectors.toList());
    }

    public void waitLocatorToHaveCount(String locator, Integer count, double timeout) {
        assertThat(getElement(locator)).hasCount(count, new LocatorAssertions.HasCountOptions().setTimeout(timeout));
    }

    public boolean isLocatorVisibleOnPage(String locator) {
        return getElement(locator).isVisible();
    }

}
