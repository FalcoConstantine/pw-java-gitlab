package com.webstore.pages.login;

import com.microsoft.playwright.Page;
import com.webstore.pages.account.AccountPage;
import io.qameta.allure.Step;

import static com.webstore.pages.Locators.LoginPageLocators.*;

public class LoginPage {

    private Page page;

    public LoginPage(Page page) {
        this.page = page;
    }

    @Step("Get title of Login page")
    public String getLoginPageTitle() {
       return page.title();
    }

    @Step("Visibility of the forgotten password link")
    public boolean isForgotPasswordLinkVisible() {
        return page.locator(forgotPwdLink).isVisible();
    }

    @Step("Fill username field: '$appUsername'")
    public LoginPage fillEmailFiled(String appUserName) {
        page.fill(emailId, appUserName);
        return new LoginPage(page);
    }

    @Step("Fill password field: '$appPassword'")
    public LoginPage fillPasswordFiled(String appPassword) {
        page.fill(password, appPassword);
        page.click(loginBtn);
        return new LoginPage(page);
    }

    @Step("Click Login button")
    public AccountPage clickLoginButton() {
        page.click(loginBtn);
        return new AccountPage(page);
    }
}