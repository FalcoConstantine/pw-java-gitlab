## Executing
## Run suite
```
mvn clean test -Dsurefire.suiteXmlFiles=regression.xml
```

## Reporting
## Generate reports
```
allure generate allure-results  --clean -o allure-report
```

## Open reports
```
allure open allure-report
```